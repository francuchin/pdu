<?php

    class Usuario_model extends Model{
        
        function __construct() {
            parent::__construct();
        }
        
        function getDatos($usuario){
            return $this->db->select('*', $this->tabla, "usuario='$usuario'")[0];
        }
        function existeEmail($email){
            return ($this->db->select('id', $this->tabla, "email='$email'")[0] != null);
        }
        
        function existeUsuario($usuario){
            return ($this->db->select('id', $this->tabla, "usuario='$usuario'")[0] != null);
        }
        
        function getUsuario($email){
            return $this->db->select('usuario', $this->tabla, "email='$email'")[0]['usuario'];
        }
        
        
        
        function compararPassword($password, $usuario){
            $p_correcta = $this->db->select('password', $this->tabla, "usuario='$usuario'")[0]['password'];
            return ($p_correcta == md5($password));
        }
        
        /* No se necesitan usuarios
        
        function getPerfil($id){
            return $this->db->select('imagen', 'usuario_imagen', "id_usuario='$id' ORDER BY `usuario_imagen`.`created_at` DESC LIMIT 0 , 1")[0];
        }
        function cambioImagen($id, $imagen){
            $data["id_usuario"] = $id;
            $data["imagen"] = $imagen;
            return $this->db->insert('usuario_imagen', $data);
        }
        function getConImagen($id = null){
            return $this->db->select('usuario.*, i.imagen', 'usuario, usuario_imagen i', "usuario.id = i.id_usuario AND i.id = (SELECT id FROM usuario_imagen ui WHERE usuario.id = ui.id_usuario GROUP BY ui.id_usuario ORDER BY ui.created_at DESC)");
        }
        function getMD5($id=null){
            if(isset($id)){
                return $this->db->select('*', $this->tabla, "MD5(id)='$id'")[0];
            }else{
                return $this->db->select('MD5(id)', $this->tabla);
            }
        }
        */
    }

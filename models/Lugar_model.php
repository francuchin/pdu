<?php

    class Lugar_model extends Model{
        
        function __construct() {
            parent::__construct();
        }
        function guardar($data){
            /*lugar*/
            $lugar["nombre"] = $data["nombre"];
            $lugar["articulo"] = $data["articulo"];
            $lugar["latitud"] = $data["latitud"];
            $lugar["longitud"] = $data["longitud"];
            $id = parent::guardar($lugar);
            /*imagenes*/
            if(isset($data["imagenes"])){
                $imagen["id_lugar"] = $id;
                foreach($data["imagenes"] as $img){
                    $imagen["imagen"] = $img;
                    $this->db->insert("lugar_imagen", $imagen);
                }
            }
            return $id;
        }
        
        function eliminar($id){
            $imagenes = $this->db->select("id","lugar_imagen", "id_lugar=".$id, true);
            for($x = 0; $x < sizeof($imagenes); $x++){
                   $this->eliminarImagen($imagenes[$x]['id']);
            }
            $puntuacion = $this->db->delete("lugar_puntuacion", "id_lugar=".$id, true);
            return ((parent::eliminar($id) == $imagenes) == $puntuacion);
        }
        
        function eliminarImagen($idImagen){
            $this->db->delete("descriptores","id_imagen=".$idImagen, true);
            unlink(".".$this->db->select("imagen","lugar_imagen","id=".$idImagen)[0]['imagen']);
            return $this->db->delete("lugar_imagen", "id=".$idImagen, true);
        }
        
        function agregarImagen($id, $imagen, $esCategoria=false){
            $data["id_lugar"] = $id;
            $data["imagen"] = $imagen;
            if($esCategoria)
                $data["esCategoria"] = 1;
            return $this->db->insert("lugar_imagen", $data);
        }
        
        function getimagenes($id){
            return $this->db->select('*', "lugar_imagen",'id_lugar='.$id.' AND lugar_imagen.esCategoria=0 ORDER BY RAND()');
        }
        
        function getrandomimage($id=null){
            if(isset($id))
                $imagenes = $this->db->select('*', "lugar_imagen",'id_lugar='.$id.' AND lugar_imagen.esCategoria=0 ORDER BY RAND() LIMIT 0,1;');
            else
                $imagenes = $this->db->select('*', "lugar_imagen", 'lugar_imagen.esCategoria=0 ORDER BY RAND() LIMIT 0,1;');
            if(sizeof($imagenes) < 1)
                return null;
            else
                return $imagenes[0]["imagen"];
        }
        
        function busquedaWeb($busqueda){
            return $this->db->select('CONCAT("'.URL.'lugar/",id) as url, nombre', 'lugar','nombre Like "%'.$busqueda.'%"');
        }
        
        function getpuntuaciones($id){
            return $this->db->select('*', "lugar_puntuacion","comprobado='ok' AND id_lugar=".$id);
        }
        
        function calificar($id,$email, $usuario, $comentario, $puntos,$claveComentario){
            $data['id_lugar'] = $id;
            $data['user_email'] = $email;
            $data['user_name'] = $usuario;
            $data['puntuacion'] = $puntos;
            $data['comentario'] = $comentario;
            $data['comprobado'] = $claveComentario;
            //return $data;
            //
            return $this->db->insert("lugar_puntuacion",$data);
        }
        function comprobarComentario($claveComentario){
             $data['comprobado'] = "ok";
            $this->db->update("lugar_puntuacion", $data, "comprobado='".$claveComentario."'");
        }
        function crearTabla($tabla){
            $data['nombre'] = $tabla->nombre;
            $data['id_lugar'] = $tabla->lugar;
            $id_tabla = $this->db->insert("lugar_tabla",$data);
            $fila = 1;
            $columna = 0;
            $celda['id_tabla'] = $id_tabla; 
            foreach($tabla->titulos as $titulo){
                $celda['fila'] = 0;
                $celda['columna'] = $columna;
                $celda['valor'] = $titulo;
                $this->db->insert("tabla_celda",$celda);
                $columna++;
            }
            foreach($tabla->filas as $row){
                $celda['fila'] = $fila;
                $columna = 0;
                foreach($row as $dato){
                    $celda['columna'] = $columna;
                    $celda['valor'] = $dato;
                    $this->db->insert("tabla_celda",$celda);
                    $columna++;
                }
                $fila++;
            }
            return $id_tabla;
        }
        function getTablas($id){
            return $this->db->select('*', "lugar_tabla","id_lugar=".$id);
        }
        function getTabla($id){
            return $this->db->select('*', "lugar_tabla","id=".$id)[0];
        }
        function getCeldasTabla($id){
            return $this->db->select('*', "tabla_celda","id_tabla=".$id);
        }
        function modificarTabla($tabla){
            $data['nombre'] = $tabla->nombre;
            $data['id_lugar'] = $tabla->lugar;
            $this->db->update("lugar_tabla",$data, "id=".$tabla->id);
            $fila = 1;
            $columna = 0;
            $this->db->delete("tabla_celda", "id_tabla=".$tabla->id, true);
            $celda['id_tabla'] = $tabla->id; 
            foreach($tabla->titulos as $titulo){
                $celda['fila'] = 0;
                $celda['columna'] = $columna;
                $celda['valor'] = $titulo;
                $this->db->insert("tabla_celda",$celda);
                $columna++;
            }
            foreach($tabla->filas as $row){
                $celda['fila'] = $fila;
                $columna = 0;
                foreach($row as $dato){
                    $celda['columna'] = $columna;
                    $celda['valor'] = $dato;
                    $this->db->insert("tabla_celda",$celda);
                    $columna++;
                }
                $fila++;
            }
            return $id_tabla;
        }
        function eliminarTabla($id){
            $this->db->delete("tabla_celda", "id_tabla=".$id, true);
            return $this->db->delete("lugar_tabla", "id=".$id, true);
        }
        /*IMAGEN*/
        function getImagenesParaComparar(){
            return $this->db->select('lugar_imagen.id_lugar, lugar_imagen.imagen, lugar.nombre, lugar.articulo', "lugar_imagen, lugar", "lugar_imagen.id_lugar=lugar.id AND lugar_imagen.esCategoria=0");
        }
        /*--- CATEGORIAS ---*/
        function getCategoria($id){
             return $this->db->select('*', "categoria", "id=".$id)[0];
        }
        function getCategorias($id){
            if(!isset($id))
                return $this->db->select('*', "categoria");
            else
                return $this->db->select('categoria.*', "categoria, lugar_categoria", "categoria.id=lugar_categoria.id_categoria AND id_lugar=".$id);
        }
        function getAllCategorias(){
            return $this->db->select('categoria.*, lugar_imagen.imagen', "categoria, lugar_imagen", "categoria.id=lugar_imagen.id_lugar AND lugar_imagen.esCategoria=1");
        }
        function getIdCategoria($nombre){
            if(!isset($nombre))
                return null;
            else
                return $this->db->select('categoria.id', "categoria", "categoria.nombre='".strtolower($nombre)."'")[0]['id'];
        }
        function getNombreCategoriaByImagen($id){
            return $this->db->select('nombre', "categoria, lugar_imagen", "lugar_imagen.esCategoria=1 AND lugar_imagen.id_lugar= categoria.id AND lugar_imagen.id=".$id)[0]['nombre'];
        }
        function addCategoria($nombre){
            $data['nombre'] = strtolower($nombre);
            $id = $this->db->insert('categoria', $data);
            return $id;
        }
        function idCateg($nombre){
            return $this->db->select('id','categoria', "nombre='".strtolower($nombre)."'")[0];
        }
        function setCategoria($id, $idCat){
            $data['id_categoria'] = $idCat;
            $data['id_lugar'] = $id;
            return $this->db->insert('lugar_categoria', $data);
        }
        function unsetCategoria($id, $idCat){
            return $this->db->delete('lugar_categoria', "id_categoria=".$idCat." AND id_lugar=".$id , true);
        }
        function getIdImagenCategoria($id){
            return $this->db->select('*', 'lugar_imagen', 'esCategoria=1 AND id_lugar='.$id)[0];
        }
        function deleteCategogia($idCat){
            $this->db->delete('lugar_categoria', "id_categoria=".$idCat , true);
            return $this->db->delete('categoria', 'id='.$idCat, true);
        }
        function listarLugaresxCategorias($idCat){
            return $this->db->select('lugar.nombre, lugar.id','lugar_categoria, lugar', "id_categoria=".$idCat." AND lugar_categoria.id_lugar=lugar.id");
        }
        function getImagenbyID($id){
            $imagen = $this->db->select('imagen', 'lugar_imagen', 'id='.$id);
            if(!isset($imagen)) return "/public/media/logo_pdu.png";
            if(!isset($imagen[0])) return "/public/media/logo_pdu.png";
            if(!isset($imagen[0]['imagen'])) return "/public/media/logo_pdu.png";
            return $imagen[0]['imagen'];
        }
        function getLugarByImagenId($id){
            $imagen = $this->db->select('id_lugar', 'lugar_imagen', 'id='.$id);
            if(!isset($imagen)) return 0;
            if(!isset($imagen[0])) return 0;
            if(!isset($imagen[0]['id_lugar'])) return 0;
            return $imagen[0]['id_lugar'];
        }
        function imagenEsCategoria($id){
            return $this->db->select('esCategoria', 'lugar_imagen', 'id='.$id)[0]['esCategoria'];
        }
        function getlugaresdistancia($latitud, $longitud){
            return $this->db->select('lugar.nombre,
                    ROUND((acos( sin( radians('.$latitud.') ) * sin( radians( lugar.latitud ) ) +
                    cos( radians('.$latitud.') ) * cos( radians( lugar.latitud ) ) *
                    cos( radians('.$longitud.') - radians( lugar.longitud ) ) ) *6378) 
                    *1000) AS distancia', 'lugar', '1=1 HAVING distancia<1000 Order By distancia');
        }
    }

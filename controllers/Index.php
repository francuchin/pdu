<?php
require_once './controllers/Lugar.php';
require_once './controllers/Usuario.php';

    class Index extends Controller{
        
        function __construct() {
            parent::__construct();
        }
        
        function index(){
                $l = new Lugar();
                $this->view->lugares = $l;
                $this->view->fondo = $l->getImageR();
                $this->view->indice = true;
                $this->view->render($this,'dashboard');
        }
    }
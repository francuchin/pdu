<?php

    class Lugar extends Controller{
        function __construct() {
            parent::__construct();
        }
        function extender(){
            var_dump();
        }
        function nuevo(){
            if(Session::is_admin())
                return $this->view->render($this,'nuevo');
            else
                return $this->Redirect('');
        }
        function categorias(){
            if(!Session::is_admin())
                return $this->Redirect('');
            if ($_SERVER['REQUEST_METHOD'] == POST && 
                (isset($_POST['nombre']) || isset(json_decode(file_get_contents("php://input"))->nombre))) {
                $this->addCategoria($_POST['nombre']);
            }
            $this->view->categorias = $this->model->getAllCategorias();
            return $this->view->render($this,'categorias');
        }
        function guardar(){
            if( isset($_POST["lugar-nombre"]) && 
                isset($_POST["lugar-ubicacion-latitud"]) && 
                isset($_POST["lugar-ubicacion-longitud"]) &&
                isset($_POST["lugar-articulo"]) ){
                    
                $data["nombre"] = addslashes($_POST["lugar-nombre"]);
                $data["latitud"] = addslashes($_POST["lugar-ubicacion-latitud"]);
                $data["longitud"] = addslashes($_POST["lugar-ubicacion-longitud"]);
                $data["articulo"] = addslashes($_POST["lugar-articulo"]);
                $aux = 0;
                
                foreach($_POST as $key => $value){
                    if(isset(explode('galeria_imagen_',$key)[1])){
                        $ruta = $this->guardarArchivoImagen($value);
                        if($ruta != "error" && $ruta != "invalida")
                            $data["imagenes"][$aux] = $ruta;
                        $aux++;
                    } 
                }
                $id = $this->model->guardar($data);
                $this->generarDescriptores($id);
                return $this->index($id);
            }
            echo "Error";
            return false;
        }
        private function descriptores(){
            $list = $this->model->getidlist();
            foreach($list as $l){
                $this->generarDescriptores($l['id']);
            }
        }
        function lugaresXubicacion($latitud, $longitud){
            
        }
        private function generarDescriptores($id_lugar){
            $imagenes = $this->model->getimagenes($id_lugar);
            foreach ($imagenes as $img) {
                $this->generarDescriptorImagen($img['id'], $img['imagen']);
            }
        }
        private function generarDescriptorImagen($id, $imagen){
            $Generador = new imagenes();
            $Generador->generarDescriptor($id, $imagen);
        }
        function index($id = null){
            if(!isset($id)) 
                return $this->Redirect('');
             if(!$this->model->existe($id))
                return $this->Redirect('');
            $this->view->lugar = $this->model->get($id);
            $this->view->lugar["imagenes"] = $this->model->getimagenes($id);
            $this->view->comentarios = $this->model->getpuntuaciones($id);
            if($this->view->comentarios == null)
                $calificacion = 0;
            else{
                $calificacion = 0;
                foreach($this->view->comentarios as $com){
                    $calificacion += intval($com['puntuacion']);
                }
                $calificacion = $calificacion/(sizeof($this->view->comentarios));
            }
            $this->view->lugar["calificacion"] = $calificacion;
            $this->view->lugar['tablas'] = $this->getTablas($id);
            return $this->view->render($this,'index');
        }
        function eliminar(){
            header('Content-Type: application/json');
            if(!Session::is_admin()){
                $data = ["resultado" => "false"];
                echo json_encode($data);
                return json_encode($data);
            }
            if(isset($_POST["id"])){
                $imagenes = $this->model->getImagenes($_POST["id"]);
                for($x = 0; $x < sizeof($imagenes); $x++){
                   unlink(".".$imagenes[$x]['imagen']);
                }
                $data = $this->model->eliminar($_POST["id"]);
                echo json_encode($data);
                return json_encode($data);
            }
            $data = ["resultado" => "false"];
            echo json_encode($data);
        }
        function getTablas($id){
            $tablas = $this->model->getTablas($id);
            $x = 0;
            $resultado;
            if($tablas[0]){
                foreach($tablas as $tabla){
                    $resultado[$x]['nombre'] = $tabla['nombre'];
                    $resultado[$x]['id'] = $tabla['id'];
                    $resultado[$x]['id_lugar'] = $tabla['id_lugar'];
                    $resultado[$x]['mostrar'] = $tabla['mostrar'];
                    $celdas = $this->model->getCeldasTabla($tabla['id']);
                    foreach($celdas as $celda){
                        if($celda['fila'] == 0){
                            $resultado[$x]['titulos'][$celda['columna']] = $celda['valor'];
                        }else{
                            $resultado[$x]['filas'][$celda['fila']][$celda['columna']] = $celda['valor'];
                        }
                    }
                    $x++;
                }
            }else{
                return null;
            }
            return $resultado;
        }
        private function getTabla($idTabla){
            if(!isset($idTabla)) 
                return null;
            $tabla = $this->model->getTabla($idTabla);
            if(!isset($tabla)) 
                return null;
            $resultado['nombre'] = $tabla['nombre'];
            $resultado['id'] = $tabla['id'];
            $resultado['id_lugar'] = $tabla['id_lugar'];
            $resultado['mostrar'] = $tabla['mostrar'];
            $celdas = $this->model->getCeldasTabla($idTabla);
            foreach($celdas as $celda){
                if($celda['fila'] == 0){
                    $resultado['titulos'][$celda['columna']] = $celda['valor'];
                }else{
                    $resultado['filas'][$celda['fila']][$celda['columna']] = $celda['valor'];
                }
            }
            return $resultado;
        }
        function crearTabla($id = null, $tabla = null){
            if ($_SERVER['REQUEST_METHOD'] == POST) {
                $tabla = json_decode(file_get_contents("php://input"));
                return $this->model->crearTabla($tabla->tabla);
            }
             if(!Session::is_admin())
                 return $this->Redirect('');
             if(!$this->model->existe($id))
                return $this->Redirect('');
             if(isset($tabla)) {
                 $tabla = $this->getTabla($tabla);
                 if(isset($tabla['id_lugar']) && $tabla['id_lugar'] == $id)
                    $this->view->tabla = $tabla;
             }
             $this->view->lugar = $this->model->get($id);
             $this->view->lugar["imagenes"] = $this->model->getimagenes($id);
             return $this->view->render($this, 'crearTabla');
        }
        function modificarTabla(){
            if ($_SERVER['REQUEST_METHOD'] == POST) {
                $tabla = json_decode(file_get_contents("php://input"));
                return $this->model->modificarTabla($tabla->tabla);
            }
        }
        function eliminarTabla($id){
            if ($_SERVER['REQUEST_METHOD'] == POST) {
                return $this->model->eliminarTabla($id);
            }
        }
        /*web*/
        function editar($id = null){
            
            if(!Session::is_admin())
                return $this->Redirect('');
                
            if ($_SERVER['REQUEST_METHOD'] == POST) {
                
                $result = $this->update();
                if($result == true){
                    return $this->Redirect("lugar/".$_POST['id']);
                }
                return $this->Redirect('');
            }
            
            if(!isset($id)) 
                return $this->Redirect('');
             if(!$this->model->existe($id))
                return $this->Redirect('');
            $this->view->lugar = $this->model->get($id);
            $this->view->fondo = $this->getImageR($id);
            return $this->view->render($this,'editar');
        }
        function editarGaleria($id = null){
            if(!Session::is_admin())
                return $this->Redirect('');
                
            if ($_SERVER['REQUEST_METHOD'] == POST) {
               $result = ($this->borrarImagenes() == true && $this->agregarImagenes());
                if($result == true){
                    return $this->Redirect("lugar/".$_POST['id']);
                }
                return $this->Redirect('');
            }
            
            if(!isset($id)) 
                return $this->Redirect('');
                
             if(!$this->model->existe($id))
                return $this->Redirect('');
                
            $this->view->lugar = $this->model->get($id);
            $this->view->fondo = $this->getImageR($id);
            $this->view->imagenes = $this->model->getimagenes($id);
            $this->view->id = $id;
            
            return $this->view->render($this,'editarGaleria');
        }
        function comentar(){
            if( isset($_POST["id_lugar"]) &&
                isset($_POST["puntuacion"]) &&
                isset($_POST["comentario"]) ){
                    
                if(!$this->model->existe($_POST["id_lugar"]))
                    return $this->Redirect('');
                    
                if(!isset($_POST["email"])){
                    $_POST["email"] = "Anonimo";
                }
                if(!isset($_POST["nombre"])){
                    $_POST["nombre"] = "Anonimo";
                }
                $claveComentario = $_POST["id_lugar"]."_".md5(rand (0 , 1000));
                $this->calificar($_POST["id_lugar"],$_POST["email"],$_POST["nombre"] ,$_POST["comentario"], $_POST["puntuacion"], $claveComentario);
                $utils=new Emails();
                $res=$utils->enviarEmailActivador($_POST["email"],$_POST["nombre"],$claveComentario);
                return $this->Redirect('lugar/'.$_POST["id_lugar"]);
                }
             return $this->Redirect('');
        }
        function comprobarComentario($claveComentario){
            $this->model->comprobarComentario($claveComentario);
            $id = explode( '_', $claveComentario);
            return $this->Redirect('lugar/'.$id[0]);
        }
        /*api*/
        function getImagenes($id=null){
            header('Content-Type: application/json');
            echo json_encode($this->model->getImagenes($id));
            return;
        }
        function getImagenesArray($id=null){
            header('Content-Type: application/json');
            $array = [];
            $imagenes = $this->model->getImagenes($id);
            for($x = 0; $x < sizeof($imagenes); $x++){
                array_push($array, $imagenes[$x]['imagen']);
            }
            echo json_encode($array);
            return;
        }
        function getLugarListado(){
            header('Content-Type: application/json');
            $data = $this->model->get();
            $result=[];
            foreach ($data as $l) {
                $r['id'] = $l['id']; 
                $r['nombre'] = $l['nombre'];
                $r['articulo'] = $l['articulo']; 
                $r['imagen'] = $this->model->getrandomimage($l['id']);
                array_push($result,$r);
                $r = [];
            }
            echo json_encode($result);
        }
        function getLugar($id = null){
            header('Content-Type: application/json');
            $result=[];
            if(!isset($id)){
                echo json_encode($result);
                return;
            }
            $data = $this->model->get($id);
            if(!isset($data)){
                echo json_encode($result);
                return;
            }
            $result['id'] = $data['id']; 
            $result['nombre'] = $data['nombre'];
            $result['articulo'] = $data['articulo']; 
            $result['latitud'] = $data['latitud']; 
            $result['longitud'] = $data['longitud']; 
            $imagenes = $this->model->getimagenes($data['id']);
            if(sizeof($imagenes > 0)){
                $result['imagen'] = $imagenes[0]['imagen'];//$this->model->getrandomimage($data['id']);
                $result['imagen2'] = $imagenes[0]['imagen']; //$this->model->getrandomimage($data['id']);
                if(sizeof($imagenes > 1)){
                     $result['imagen2'] = $imagenes[1]['imagen']; 
                }
            }else{
                $result['imagen'] = null;
                 $result['imagen2'] = null;
            }
            /*
            $result['imagen'] = $this->model->getrandomimage($data['id']);
            $result['imagen2'] = $this->model->getrandomimage($data['id']);
            */
            echo json_encode($result);
        }
        function edit(){
            header('Content-Type: application/json');
            if(!Session::is_admin()){
                $data = ["resultado" => "false",
                        "error" => "no admin"];
                echo json_encode($data);
                return json_encode($data);
            }
            $data = ["resultado" => $this->update()];
            echo json_encode($data);
            return json_encode($data);
        }
        function getImageR($id = null){ // prueba
               $data = ["resultado" => "ok",
                        "imagen" => $this->model->getrandomimage($id)];
                if ($_SERVER['REQUEST_METHOD'] == POST) {
                    header('Content-Type: application/json');
                    echo json_encode($data);
                }
                return $data;
        }
        function obtenerComentarios($id){
            header('Content-Type: application/json');
            $result = $this->getPuntuaciones($id);
            echo json_encode($result);
        }
        function obtenerComentariosConImagen($id){
            header('Content-Type: application/json');
            $result = $this->getPuntuaciones($id);
            for($x = 0; $x < sizeof($result); $x++){
                $result[$x]["imagen"] = $this->getimagenDesdeEmail($result[$x]["user_email"]);
            }
            echo json_encode($result);
        }
        private function getimagenDesdeEmail($email=null){
            header('Content-Type: application/json');
            if(!isset($email)) return "https://pdu-francuchin.c9users.io/public/media/logo_pdu.png";
            $jsonPicasa = file_get_contents("https://picasaweb.google.com/data/entry/api/user/".$email."?alt=json");
            $thumbnail = 'gphoto$thumbnail';
            $t = '$t';
            if(json_decode($jsonPicasa) == null) return "https://pdu-francuchin.c9users.io/public/media/logo_pdu.png";
            if(json_decode($jsonPicasa)->entry == null) return "https://pdu-francuchin.c9users.io/public/media/logo_pdu.png";
            if(json_decode($jsonPicasa)->entry->$thumbnail == null) return "https://pdu-francuchin.c9users.io/public/media/logo_pdu.png";
            if(json_decode($jsonPicasa)->entry->$thumbnail->$t == null) return "https://pdu-francuchin.c9users.io/public/media/logo_pdu.png";
            return json_decode($jsonPicasa)->entry->$thumbnail->$t;
        }
        function busquedaWeb($busqueda=null){
            header('content-type: application/json; charset=utf-8');
            header("access-control-allow-origin: *");
            $result['items'] = $this->model->busquedaWeb($busqueda);
            echo json_encode($result);
        }
        /*function*/
        private function update(){
            
            if(!isset($_POST["id"]))
                 return false;
            
            if(!$this->model->existe($_POST["id"]))
                 return false;
                 
            $data = [];
            
            foreach($_POST as $key => $value){
                if( $key == "nombre" || $key == "articulo" ||
                    $key == "latitud" || $key == "longitud")
                    $data[$key] = addslashes($value);
            }
            
            return $this->model->editar($_POST["id"],$data);
        }
        private function borrarImagenes(){
            if(!isset($_POST["id"]))
                 return false;
            if(!$this->model->existe($_POST["id"]))
                 return false;
            $result = true;
            if (isset($_POST['imagenes_borrar'])) {
                $imagenes = $_POST['imagenes_borrar'];
                for ($i=0; $i<count($imagenes); $i++) {
                   $result = ($this->model->eliminarImagen($imagenes[$i]) == true && $result == true);
                }
            }
            return $result;
        }
        private function agregarImagenes(){
            if(!isset($_POST["id"]))
                 return false;
            if(!$this->model->existe($_POST["id"]))
                 return false;
            $result = true;
            if (isset($_POST['imagenes_agregar'])) {
                $imagenes = $_POST['imagenes_agregar'];
                for ($i=0; $i<count($imagenes); $i++) {
                   $rutaImg = $this->guardarArchivoImagen($imagenes[$i]);
                   $idImg = $this->model->agregarImagen($_POST["id"], $rutaImg);
                   $this->generarDescriptorImagen($idImg, $rutaImg);
                   $result = ($idImg && $result == true);
                }
            }
            return $result;
        }
        private function calificar($id,$email, $usuario, $comentario, $puntos,$claveComentario){
            return $this->model->calificar($id,$email, $usuario, $comentario, $puntos,$claveComentario);
        }
        private function getPuntuaciones($id){
            return $this->model->getpuntuaciones($id);
        }
        function llamarfuncion($nombre = "", $parametros = []){
            $function = new ReflectionMethod($this, $nombre);
            return $function->invokeArgs($this, $parametros);
        }      
        /*IMAGEN*/
        function leerimagen(){
            header('Content-Type: application/json');
            if ($_SERVER['REQUEST_METHOD'] == POST) {
                $img = json_decode(file_get_contents("php://input"))->imagen;
                $imagenes = $this->getImagenesParaComparar();
                $comparador = new Comparador(8);
                $resultado = $comparador->comparar_img_array($img , $imagenes);
                echo json_encode($resultado);
                return json_encode($resultado);;
            }
            echo  "ERROR";
            return false;
        }
        function leerimagenbase64(){
            header('Content-Type: application/json');
            if(isset($_POST['imagen'])){
              $base = $_POST['imagen'];
            }else{
                if(isset(json_decode(file_get_contents("php://input"))->imagen)){
                    $base = json_decode(file_get_contents("php://input"))->imagen;
                }else{
                    echo "ERROR recibiendo la imagen";
                    return;
                }
            }
            $img = ".".$this->guardarArchivoImagen($base);
            $imagenes = $this->getImagenesParaComparar();
            $comparador = new Comparador(8);
            $resultado = $comparador->comparar_img_array($img , $imagenes);
            unlink($img);
            echo json_encode($resultado);
            return;
        }
        function compararImagen(){
            $Generador = new imagenes();
            echo $Generador->compararImagen('/imagenes/4ae7e84f34b74570ff328ea84ae314df.jpg');
        }
        private function getImagenesParaComparar(){
            return $this->model->getImagenesParaComparar();
        }
        /*--- CATEGORIAS ---*/
        function unsetCategoria($id){
            $categoria = $_POST['categoria'];
            if(!isset($categoria)){
                $categoria = json_decode(file_get_contents("php://input"))->categoria;
            }
            $idCat = $this->model->idCateg($categoria);
            if(isset($idCat))
                return $this->model->unsetCategoria($id, $idCat['id']);
            return null;
        }
        function setCategoria($id){
            $categoria = $_POST['categoria'];
            if(!isset($categoria)){
                $categoria = json_decode(file_get_contents("php://input"))->categoria;
            }
            if(isset($categoria)){
                $idCat = $this->model->idCateg($categoria);
                if(!isset($idCat))
                    $idCat = $this->model->addCategoria($categoria);
                echo $this->model->setCategoria($id, $idCat['id']);
                return;
            }
            echo "Error";
        }
        function getCategorias($id=null){
            return $this->model->getCategorias($id);
        }
        function addCategoria($nombre=null){
            if(!Session::is_admin()){
                $data = ["resultado" => "false",
                        "error" => "no admin"];
                echo json_encode($data);
                return json_encode($data);
            }
            if(isset($_POST['imagen'])){
              $imagen = $_POST['imagen'];
            }else{
                if(isset(json_decode(file_get_contents("php://input"))->imagen)){
                    $imagen = json_decode(file_get_contents("php://input"))->imagen;
                }else{
                    $data = ["resultado" => "Error imagen"];
                    echo json_encode($data);
                    return $data;
                }
            }
            if(!isset($nombre)){
                $nombre = $_POST['nombre'];
            }
            if(!isset($nombre)){
                 if(isset(json_decode(file_get_contents("php://input"))->nombre)){
                     $nombre = json_decode(file_get_contents("php://input"))->nombre;
                 }else{
                    $data = ["resultado" => "Error nombre"];
                    echo json_encode($data);
                    return $data;
                 }
            }
            $rutaImg = $this->guardarArchivoImagen($imagen);
            $idCategoria = $this->model->addCategoria($nombre);
            $imagen_id = $this->model->agregarImagen($idCategoria, $rutaImg, true);
            $this->generarDescriptorImagen($imagen_id, $rutaImg);
            $data = ["resultado" => $idCategoria];
            echo json_encode($data);
            return $data;
        }
        function webdeletecategoria($nombre){
            $this->deleteCategogia($nombre);
            return $this->Redirect('lugar/categorias');
        }
        function deleteCategogia($nombre){
            header('Content-Type: application/json');
            if(!Session::is_admin()){
                $data = ["resultado" => "false",
                        "error" => "no admin"];
                return json_encode($data);
            }
            $id = $this->model->getIdCategoria($nombre);
            $img = $this->model->getIdImagenCategoria($id);
            if(isset($img)){
                $idImagen = $img['id'];
                $rutaImagen = $img['imagen'];
                $this->model->eliminarImagen($idImagen);
                unlink('./'.$rutaImagen);
            }
            if(!isset($id)){
                $data = ["resultado" => "false",
                        "error" => "no existe categoria"];
                return json_encode($data);
            }
            return $this->model->deleteCategogia($id);
        }
        function listarCategorias($id=null){
            header('Content-Type: application/json');
            echo json_encode($this->getCategorias($id));
        }
        function listarLugaresxCategorias($categoria){
            header('Content-Type: application/json');
            $idCat = $this->model->getIdCategoria($categoria);
            $lugares = $this->model->listarLugaresxCategorias($idCat);
            for($x = 0; $x<sizeof($lugares); $x++){
                $lugares[$x]['imagen'] = $this->model->getrandomimage($lugares[$x]['id']);
            }
            $resultado = $this->model->getCategoria($idCat);
            $resultado['lista'] = $lugares;
            echo json_encode($resultado);
        }
        function listarLugaresxCategoriasxId($categoria){
            header('Content-Type: application/json');
            $lugares = $this->model->listarLugaresxCategorias($categoria);
            for($x = 0; $x<sizeof($lugares); $x++){
                $lugares[$x]['imagen'] = $this->model->getrandomimage($lugares[$x]['id']);
            }
            $resultado = $this->model->getCategoria($categoria);
            $resultado['lista'] = $lugares;
            echo json_encode($resultado);
        }
        function listasResultadoBusquedaParaApp(){
            header('Content-Type: application/json');
            if(isset($_POST['imagen'])){
              $base = $_POST['imagen'];
            }else{
                if(isset(json_decode(file_get_contents("php://input"))->imagen)){
                    $base = json_decode(file_get_contents("php://input"))->imagen;
                }else{
                    echo "ERROR recibiendo la imagen";
                    return;
                }
            }
            $img = $this->guardarArchivoImagen($base);
            $Generador = new imagenes();
            $R = $Generador->compararImagen('/'.$img);
            unlink('./'.$img);
            $json['resultado']['id_lugar'] = $this->model->getLugarByImagenId($R);
            $json['resultado']['es_categoria'] = $this->model->imagenEsCategoria($R);
            echo json_encode($json);
        }
        function listasResultadoBusquedaParaAppUbicado(){
            header('Content-Type: application/json');
            $latitud = $_POST['latitud'];
            $longitud = $_POST['longitud'];
            if(isset($_POST['imagen'])){
              $base = $_POST['imagen'];
            }else{
                if(isset(json_decode(file_get_contents("php://input"))->imagen)){
                    $base = json_decode(file_get_contents("php://input"))->imagen;
                }else{
                    echo "ERROR recibiendo la imagen";
                    return;
                }
            }
            if(!isset($latitud))
                if(isset(json_decode(file_get_contents("php://input"))->latitud))
                    $latitud = json_decode(file_get_contents("php://input"))->latitud;
            if(!isset($longitud))
                if(isset(json_decode(file_get_contents("php://input"))->longitud))
                    $longitud = json_decode(file_get_contents("php://input"))->longitud;
            $img = $this->guardarArchivoImagen($base);
            $Generador = new imagenes();
            $R = $Generador->compararImagenUbicada('/'.$img, $latitud, $longitud);
            unlink('./'.$img);
            $esCategoria = $this->imagenEsCategoria($R);
            if($esCategoria)
                $json['resultado']['id_lugar'] = $this->model->getNombreCategoriaByImagen($R);
            else
                $json['resultado']['id_lugar'] = $this->model->getLugarByImagenId($R);
            $json['resultado']['es_categoria'] = $esCategoria;
            echo json_encode($json);
        }
        private function imagenEsCategoria($id){
            return $this->model->imagenEsCategoria($id);
        }
        function listasCategoriasParaApp($vacias="false"){
            header('Content-Type: application/json');
            $categorias = $this->model->getCategorias(null);
            $listadoCategorias = [];
            $listaIdLugares = $this->model->getIdList();
            $listadoImagenes = [];
            for($x = 0; $x < sizeof($listaIdLugares); $x++){
                $listadoImagenes[$listaIdLugares[$x]['id']] =  $this->model->getrandomimage($listaIdLugares[$x]['id']);
            }
            if($vacias != "false"){
                for($x = 0; $x < sizeof($categorias); $x++){
                    $listadoCategorias[$x]['nombre'] = $categorias[$x]['nombre'];
                    $listadoCategorias[$x]['id'] = $categorias[$x]['id'];
                    $imaCat = $this->model->getIdImagenCategoria($categorias[$x]['id']);
                    if(isset($imaCat))
                        $listadoCategorias[$x]['imagen'] = $imaCat['imagen'];
                    else
                        $listadoCategorias[$x]['imagen'] = null;
                    $listadoCategorias[$x]['lista'] = $this->model->listarLugaresxCategorias($categorias[$x]['id']);
                    for($j = 0; $j < sizeof($listadoCategorias[$x]['lista']); $j++){
                        if(isset($listadoCategorias[$x]['lista'][$j])){
                            if(isset($listadoCategorias[$x]['lista'][$j]['id'])){
                                $listadoCategorias[$x]['lista'][$j]['imagen'] = $listadoImagenes[$listadoCategorias[$x]['lista'][$j]['id']];
                                $calificaciones = $this->getPuntuaciones($listadoCategorias[$x]['lista'][$j]['id']);
                                $puntaje = 0;
                                $cont = 0;
                                if (is_array($calificaciones) || is_object($calificaciones))
                                    foreach($calificaciones as $cal){
                                      $puntaje+=$cal['puntuacion'];
                                      $cont++;
                                    }
                                if($cont>0)
                                $puntaje = $puntaje/$cont;
                                $listadoCategorias[$x]['lista'][$j]['calificacion'] = round($puntaje, 1);
                            }
                        }
                    }
                }
            }else{
                for($x = 0; $x < sizeof($categorias); $x++){
                    $listaActual = $this->model->listarLugaresxCategorias($categorias[$x]['id']);
                    if(isset($listaActual)){
                        $listadoCategorias[$x]['nombre'] = $categorias[$x]['nombre'];
                        $listadoCategorias[$x]['lista'] = $listaActual;
                        for($j = 0; $j < sizeof($listadoCategorias[$x]['lista']); $j++){
                            if(isset($listadoCategorias[$x]['lista'][$j])){
                                if(isset($listadoCategorias[$x]['lista'][$j]['id'])){
                                    $listadoCategorias[$x]['lista'][$j]['imagen'] = $listadoImagenes[$listadoCategorias[$x]['lista'][$j]['id']];
                                }
                            }
                        }
                    }
                }                
            }
            echo json_encode($listadoCategorias);
        }
        function getlugaresdistancia($latitud, $longitud){
            echo json_encode($this->model->getlugaresdistancia($latitud, $longitud));
        }
        function getTablasLugar($id){
             header('Content-Type: application/json');
             echo json_encode($this->getTablas($id));
        }
    }
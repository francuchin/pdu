<?php
    require_once './controllers/Index.php';
    class Usuario extends Controller{
        
        function __construct() {
            parent::__construct();
        }
        
        function signUp(){
            //nombre,usuario,email,password
            if(!Session::is_admin()){
                return $this->Redirect('');
            }
            if( isset($_POST["nombre"]) && isset($_POST["usuario"]) &&
                isset($_POST["email"]) && isset($_POST["password"])){
                if( empty($_POST["nombre"]) || empty($_POST["usuario"]) ||
                    empty($_POST["email"]) || empty($_POST["password"]) ){
                    foreach($_POST as $key => $value){
                        $this->view->{$key} = $value;
                    }
                    $this->view->error = 'faltan datos';
                    $this->view->render($this,'registro');
                    return false;
                }
                $this->registrar();
                $this->Redirect('');
            }else
                $this->view->render($this,'registro');
        }
        
        function admin(){
            return $this->view->render($this,'login');  
        }
        function registro(){
             if(!Session::is_admin()){
                return $this->Redirect('');
            }
            return $this->view->render($this,'registro');
        }
        function signIn(){
            if($this->login() == false){
                $index = new Index();
                $index->view->error = 'Error al intenar autenticarse';
                $index->view->render($this,'login');  
            }else{
                $this->Redirect('');
            }
        }
        
        private function createSession($usuario,$id){
            Session::setValue('U_NAME', $usuario);
            Session::setValue('ID', $id);
        }
        private function registrar(){
            if(!Session::is_admin()){
                return $this->Redirect('');
            }
            if( isset($_POST["nombre"]) && isset($_POST["usuario"]) &&
                isset($_POST["email"]) && isset($_POST["password"])){
                    
                    $data["nombre"] = $_POST["nombre"];
                    $data["usuario"] = $_POST["usuario"];
                    $data["email"] = $_POST["email"];
                    $data["password"] = md5($_POST["password"]);
                    
                    return $this->model->guardar($data);
                }
                return false;
        }
        private function login(){
            if( isset($_POST["usuario"]) && isset($_POST["password"])){
                if($this->model->existeUsuario($_POST["usuario"])){
                    $usuario = $_POST["usuario"];
                }else{
                    if($this->model->existeEmail($_POST["usuario"])){
                        $usuario = $this->model->getUsuario($_POST["usuario"]);
                    }else{
                        return false;
                    }
                }
                if(!$this->model->compararPassword($_POST["password"], $usuario))
                    return false;
                $response = $this->model->getDatos($usuario);
                $this->createSession($response["usuario"],$response["id"]);
                return $response['id'];
            }
            return false;
        }
        function destroySession(){
            Session::destroy();
            $this->Redirect('');
        }
        
        /* No se necesitan usuarios
        
        function index($id){
            if(!isset($id))
                return $this->Redirect('');
            $user = $this->model->get($id);
            if(!isset($user))
                return $this->Redirect('');
            $this->view->soy_yo = (Session::getValue("ID") == $id);
            $this->view->usuario = $user;
            $this->view->imagenPerfil = $this->model->getPerfil($id);
            return $this->view->render($this,'index');
        }
        function cambioImagen(){
            if( isset($_POST["id"]) && isset($_POST["imagen"]) ){
                echo $this->model->cambioImagen($_POST["id"],$_POST["imagen"]);
                return;
            }
            echo "false";
        }
        private function invitado(){
            $this->createSession('invitado', 0, 0);  
            $this->Redirect('');
        }
         /-*api*-/
        function ingresar(){
             header('Content-Type: application/json');
            $result = $this->login();
            if($result){
                echo json_encode(["token" => md5($result)]);
            }else{
                echo json_encode(['error'=>'credenciales incorrectas']);
            }
        }
        function registro(){
            header('Content-Type: application/json');
            $result = $this->registrar();
            if($result){
                echo json_encode(["token" => md5($result)]);
            }else{
                echo json_encode(['error'=>$result]);
            }
        }
        function getConImagen($id=null){
            header('Content-Type: application/json');
            $result = $this->model->getConImagen($id);
            echo json_encode($result);
        }
        
        function getMD5($id=null){
            echo json_encode($this->model->getMD5($id));
        }
        */
        
    }
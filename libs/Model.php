<?php

    class Model{
        
        function __construct() {
            $this->db = new MySQLiManager(DB_HOST,DB_USER,DB_PASS,DB_NAME);
            $this->tabla = mb_strtolower((explode('_model',get_class($this))[0]));
        }
        
        function comprobarToken($id=null){
            if(!isset($id))
                return null;
            return $this->db->select('*', $this->tabla, "MD5(id)='$id'")[0];
        }
        
        function existe($id){
            return ($this->db->select('*', $this->tabla,'id='.$id)[0] != null);
        }
        
        function guardar($data){
            return $this->db->insert($this->tabla, $data);
        }
        
        function eliminar($id){
           return $this->db->delete($this->tabla, "id=".$id, true);
        }
        
        function editar($id, $data){
            return $this->db->update($this->tabla, $data, "id=".$id);
        }
        
        function getidlist(){
            return $this->db->select('id', $this->tabla);
        }
        
        function get($id = null){
            if (isset($id)){
                return $this->db->select('*', $this->tabla,'id='.$id)[0];
            }else
                 return $this->db->select('*', $this->tabla);
        }
    }

<?php

class imagenes{
    function __construct() {
    }
    function generarDescriptor($id_imagen,$nombre_archivo){
        $host = DB_HOST;
        $username = DB_USER_SIFT;
        $userpass = DB_PASS_SIFT;
        $name = DB_NAME;
        $sift = CARPETA_SERVER."/libs/sift/encuadroSift";
        $nombre_archivo2 = $nombre_archivo;
        $recortado = substr($nombre_archivo2,0,strpos($nombre_archivo2,'.')-strlen($nombre_archivo2));
        $archivo_original = CARPETA_SERVER.$nombre_archivo;
        $archivo_nuevo =CARPETA_SERVER.$recortado.".pgm";
        exec("convert -filter triangle -resize 512x512 ".$archivo_original." ".$archivo_nuevo);
        $res = -1;
       // echo $sift." ".$archivo_nuevo." ".$id_imagen." ".$host." ".$username." ".$userpass." ".$name." generar<br>";
        $res=exec($sift." ".$archivo_nuevo." ".$id_imagen." ".$host." ".$username." ".$userpass." ".$name." generar");
        unlink($archivo_nuevo);
    }
    function compararImagen ($nombre_archivo){
        $host = DB_HOST;
        $username = DB_USER_SIFT;
        $userpass = DB_PASS_SIFT;
        $name = DB_NAME;
        $sift = CARPETA_SERVER."/libs/sift/encuadroSift";
        $nombre_archivo2 = $nombre_archivo;
        $recortado = substr($nombre_archivo2,0,strpos($nombre_archivo2,'.')-strlen($nombre_archivo2));
        $archivo_original = CARPETA_SERVER.$nombre_archivo;
        $archivo_nuevo =CARPETA_SERVER.$recortado.".pgm";
        exec("convert -filter triangle -resize 512x512 ".$archivo_original." ".$archivo_nuevo);
        $u=-1;
        $u = exec($sift." ".$archivo_nuevo." no_joda ".$host." ".$username." ".$userpass." ".$name);
        unlink($archivo_nuevo);
        return $u;//return tojson($u);
    }
    function compararImagenUbicada ($nombre_archivo,$latitud, $longitud){
        $host = DB_HOST;
        $username = DB_USER_SIFT;
        $userpass = DB_PASS_SIFT;
        $name = DB_NAME;
        $sift = CARPETA_SERVER."/libs/sift/encuadroSift";
        $nombre_archivo2 = $nombre_archivo;
        $recortado = substr($nombre_archivo2,0,strpos($nombre_archivo2,'.')-strlen($nombre_archivo2));
        $archivo_original = CARPETA_SERVER.$nombre_archivo;
        $archivo_nuevo =CARPETA_SERVER.$recortado.".pgm";
        exec("convert -filter triangle -resize 512x512 ".$archivo_original." ".$archivo_nuevo);
        $u=-1;
        $u = exec($sift." ".$archivo_nuevo." 0 ".$host." ".$username." ".$userpass." ".$name." ".$latitud." ".$longitud);
        unlink($archivo_nuevo);
        return $u;//return tojson($u);
    }
    
}
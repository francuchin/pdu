<?php

class Controller{
    function __construct() {
        Session::init();
        $this->view = new View();
        $this->loadModel();
    }
    function index(){
        return $this->Redirect('');
    }
    function llamarfuncion($nombre = "", $parametros = []){
        call_user_func_array($nombre , $parametros);
    }
    function verificarSession(){
       /* $token = null;
        if(isset($_POST['token'])){
            $token = $_POST['token'];
        }
        if(isset($_GET['token'])){
            $token = $_GET['token'];
        }
        if(!Session::exist()){
            if($this->model->comprobarToken($token) == null)
                return $this->Redirect('');
        }*/
    }
    function loadModel(){
        $model = get_class($this).'_model';
        $path = 'models/'.$model.'.php';
        
        if(file_exists($path)){
            require $path;
            $this->model = new $model();
        }
    }
    function Redirect($url, $permanent = false){
        header('Location: '.URL . $url, true, $permanent ? 301 : 302);
        exit();
    }
    /* api */
    function getidlist() {
        header('Content-Type: application/json');
        if ($_SERVER['REQUEST_METHOD'] == GET) {
            echo json_encode($this -> model -> getidlist());
            return;
        }
        echo json_encode(["resultado" => "error"]);
    }
    function exist($id){
        header('Content-Type: application/json');
        $existe = $this->model->existe($id);
        echo json_encode (["resultado" => $existe]);
    }
    function get($id = null){
        header('Content-Type: application/json');
        if($_SERVER['REQUEST_METHOD'] == GET){
            echo json_encode($this->model->get($id));
        return;
        }
        echo json_encode(["resultado" => "error"]);
    }
    function guardarArchivoImagen($imagen){
         //comprobar que es imagen
         $archivo_comprobacion  = $imagen;
         list(, $archivo_comprobacion) = explode(';', $archivo_comprobacion);
         list(, $archivo_comprobacion) = explode(',', $archivo_comprobacion);
         $archivo_comprobacion = base64_decode($archivo_comprobacion);
         $finfo = new finfo(FILEINFO_MIME);
         if (0 !== strcmp('image',(explode('/',$finfo->buffer($archivo_comprobacion))[0]))){
            return "invalida";
         }
         // generar nombre
         //$nombre = "imagenes/".md5(microtime()).".png";
         $nombre = "imagenes/".md5(microtime()) .".".explode(';',explode('/',$finfo->buffer($archivo_comprobacion))[1])[0];
            if(file_put_contents($nombre, $archivo_comprobacion)){
                return "/".$nombre;
            }else{
                return "error";
            }
    }
    
}
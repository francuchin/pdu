<?php
require_once('phasher.php');
class Comparador
{

	function __construct($size)
	{
		$this->size=$size;
	}
	
	public function comparar_img_array($img1, $arrayImg){
		$resultado = [];
		$x = 0;
		
		$I = PHasher::Instance();
		
		foreach($arrayImg as $img){
			$img2 = './'.$img['imagen'];
			$dif       = $I->Compare($img1, $img2);//this->calc_dif_porc_hash($hash1_str,$hash2_str);//
			$dif = 100 - $dif;
			if($dif<60){
				$resultado[$x]['id'] = $img['id_lugar'];
				$resultado[$x]['imagen'] = $img['imagen'];
				$resultado[$x]['articulo'] = "";//$img['articulo'];
				$resultado[$x]['nombre'] = $img['nombre'];
	            $resultado[$x]['diferencia'] = "$dif";
	            $x++;
			}
		}
		for($i=1;$i<$x;$i++){
            for($j=0;$j<$x-$i;$j++){
                if($resultado[$j]['diferencia']>$resultado[$j+1]['diferencia']){
                	$k=$resultado[$j+1]; 
                	$resultado[$j+1]=$resultado[$j]; 
                	$resultado[$j]=$k;
                }
            }
    	}
    	$repetidos = [];
    	$finalResultado = [];
    	for($j=0;$j< sizeof($resultado);$j++){
    		if(!in_array($resultado[$j]['id'], $repetidos)) {
    			array_push($finalResultado,$resultado[$j]);
				array_push($repetidos, $resultado[$j]['id']);
    		}
    	}
		return $finalResultado;		
	}
	
	public function comparar_img_array2($img1, $arrayImg){
		$resultado = [];
		$x = 0;
		
		$ext1      = $this->get_file_ext($img1);
		$hash1     = $this->hash_img($img1,$ext1,$this->size);
		$hash1_str = $this->concatenar_array($hash1);
		
		foreach($arrayImg as $img){
			$img2 = ".".$img['imagen'];
			$ext2      = $this->get_file_ext($img2);
			$hash2     = $this->hash_img($img2,$ext2,$this->size);
			$hash2_str = $this->concatenar_array($hash2);
			
			$dif       = $this->calc_dif_porc_hash($hash1_str,$hash2_str);
			
			if($dif<60){
				$resultado[$x]['id'] = $img['id_lugar'];
				$resultado[$x]['imagen'] = $img['imagen'];
				$resultado[$x]['articulo'] = "";//$img['articulo'];
				$resultado[$x]['nombre'] = $img['nombre'];
	            $resultado[$x]['diferencia'] = $dif;
	            $x++;
			}
		}
		for($i=1;$i<$x;$i++){
            for($j=0;$j<$x-$i;$j++){
                if($resultado[$j]['diferencia']>$resultado[$j+1]['diferencia']){
                	$k=$resultado[$j+1]; 
                	$resultado[$j+1]=$resultado[$j]; 
                	$resultado[$j]=$k;
                }
            }
    	}
    	$repetidos = [];
    	$finalResultado = [];
    	for($j=0;$j< sizeof($resultado);$j++){
    		if(!in_array($resultado[$j]['id'], $repetidos)) {
    			array_push($finalResultado,$resultado[$j]);
				array_push($repetidos, $resultado[$j]['id']);
    		}
    	}
		return $finalResultado;
	}

	public function comparar_imgs($img1,$img2)
	{
		$ext1      = $this->get_file_ext($img1);
		$hash1     = $this->hash_img($img1,$ext1,$this->size);
		$hash1_str = $this->concatenar_array($hash1);
		
		$ext2      = $this->get_file_ext($img2);
		$hash2     = $this->hash_img($img2,$ext2,$this->size);
		$hash2_str = $this->concatenar_array($hash2);
		
		$dif       = $this->calc_dif_porc_hash($hash1_str,$hash2_str);
		return $dif;
	}

	private function get_file_ext($file)
	{
		return pathinfo($file, PATHINFO_EXTENSION);
	}

	private function average($img)
	{
		$w = imagesx($img);
		$h = imagesy($img);
		$r = $g = $b = 0;
		
		for($y = 0; $y < $h; $y++)
		{
			for($x = 0; $x < $w; $x++)
			{
				$rgb = imagecolorat($img, $x, $y);
				$r  += $rgb >> 16;
			}
		}
		
		$pxls = $w * $h;
		$r = (round($r / $pxls));

		//return $r ."," . $g .",". $b;
		if($r<10)
			$r=0;
			
		if($r>245)
			$r=255;
			
		return $r; 
	} 

	private function ComputeHash($img, $avg = 100)
	{
		$w = imagesx($img);
		$h = imagesy($img);
		
		$matrix = array();		
		
		$r = $g = $b = 0;
		
		for($y = 0; $y < $h; $y++)
		{
			$fila = "";
			
			for($x = 0; $x < $w; $x++)
			{
				$rgb = imagecolorat($img, $x, $y);
				$r   = $rgb >> 16;
				
				if($r <= $avg)
					$fila = $fila."0";
				else
					$fila = $fila."1";
			}
			$matrix[$y]=$fila;
		}
		
		return $matrix;
	}

	private function concatenar_array($array)
	{
		$txt ="";
		
		for ($i=0;$i<=count($array)-1;$i++)
		{
			$tmp = $array[$i];
			$txt = $txt.$tmp;
		}
		
		return $txt;
	}

	private function convertir_BW($img)
	{
		imagefilter($img, IMG_FILTER_GRAYSCALE);
	}

	private function resizeImage($type,$file,$w,$h)
	{
		$img_original	   = null;
		
		if ($type=='png')		
			$img_original 	   = imagecreatefrompng($file);

		if ($type=='jpg')
			$img_original 	   = imagecreatefromjpeg($file);
		if ($type=='jpeg')
			$img_original 	   = imagecreatefromjpeg($file);
			
		$max_ancho    	   = $w;
		$max_alto          = $h;
		list($ancho,$alto) = getimagesize($file);
		$x_ratio 		   = $max_ancho/$ancho;
		$y_ratio 		   = $max_alto/$alto;
		
		if(($ancho<=$max_ancho)&&($alto<=$max_alto))
		{
			$ancho_final = $ancho;
			$alto_final  = $alto;
		}
		elseif (($x_ratio * $alto) < $max_alto)
		{
			$alto_final  = ceil($x_ratio * $alto);
			$ancho_final = $max_ancho;
		}
		else
		{
			$ancho_final = ceil($y_ratio * $ancho);
			$alto_final  = $max_alto;
		}
		
		$tmp			 =	imagecreatetruecolor($ancho_final,$alto_final);	
		imagecopyresampled($tmp,$img_original,0,0,0,0,$ancho_final, $alto_final,$ancho,$alto);
		imagedestroy($img_original);
		
		return $tmp;
	}

	private function hash_img($file,$type,$size)
	{
		$img  = $this->resizeImage($type,$file,$size,$size);
		$this->convertir_BW($img);
		$avg  = $this->average($img);
		$hash = $this->ComputeHash($img,$avg);
		
		return $hash;
	}

	private function calc_dif_porc_hash($h1,$h2)
	{
		$fail = 0;	
		
		for($x=0;$x <=strlen($h2)-1;$x++)
		{
			if(!isset($h1[$x]))
				$fail++;
			else
				if($h1[$x] != $h2[$x])
					$fail++;
		}
		
		$delta = ($fail*100)/strlen($h2);
		
		return $delta;
	}

}
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />

  <title>Registro</title>
  <link type="text/css" rel="stylesheet" href="/public/semantic/semantic.css">
  <script src="/public/js/jquery.min.js"></script>
  <script type="text/javascript" src="/public/js/form_registro.js"></script>
  <script type="text/javascript" src="/public/semantic/semantic.min.js"></script>
  
  <style type="text/css">
    body {
      background-color: #DADADA;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>
</head>
<body>
<div class="ui middle aligned center aligned grid">
  <div class="column">
   <a href="/">
   <h1 class="ui teal header">
      <img src="/public/media/logo_pdu.png" class="image">
      <div class="content">
       Registro
      </div>
    </h1>
    </a>
    <?php if(isset($error)){?>
      <div class="ui pointing below red basic label">
        <?=$error?>
      </div>
    <?php }?>
    <form class="ui large form" action="/Usuario/signUp" method="post">
      <div class="ui stacked segment">
        <div class="field">
          <div class="ui left icon input">
            <i class="crosshairs icon"></i>
            <input type="text" name="nombre" placeholder="Nombre*" value="<?=$nombre?>">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="usuario" placeholder="Usuario*" value="<?=$usuario?>">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="mail icon"></i>
            <input type="email" name="email" placeholder="Email*" value="<?=$email?>">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="password" placeholder="Contraseña*">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="repassword" placeholder="Repetir contraseña*">
          </div>
        </div>
        <div class="ui fluid large teal submit button">Aceptar</div>
        
      </div>
      <div class="ui error message"></div>
    </form>
  </div>
</div>

</body>

</html>
<!DOCTYPE html>
<html>
<head>
    <title>Pdu</title>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" href="/public/semantic/semantic.css" type="text/css" />
    <link rel="stylesheet" href="/public/css/estilos.css" type="text/css" />
    <script type="text/javascript" src="/public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/public/semantic/semantic.min.js"></script>
</head>
<body>
    <?php require './views/menu.php'?>
    <div class="ui container">
        <div class="ui segment">
            <div class="ui inverted dimmer">
              <div class="ui text loader">Guardando Cambios...</div>
            </div>
        <div class="previa_imagen">
            <?php if(isset($imagenPerfil)){ ?>
            <img id="imagenActual" src="<?=$imagenPerfil['imagen']?>">
            <?php }else{ ?>
            <img id="imagenActual" src="/public/media/logo_pdu.png">
            <?php } ?>
            <?php if($soy_yo){ ?>
            <div class="btn_imagen">
                <i class="photo icon" onclick="javascript:document.getElementById('archivo_imagen').click();" ></i>
                <input type="file" name="archivo_imagen" id="archivo_imagen" accept="image/*" hidden/>
            </div>
            <?php } ?>
        </div>
        <div class="content">
            <a class="header">
                <?=$usuario["nombre"]?>
            </a>
            <div class="meta">
                <span class="date"><?php 
                $timeZone = NULL;               //------ jimmiw/php-time-ago
                $timeAgo = new TimeAgo($timeZone, 'es');
                echo $timeAgo->inWords($usuario["created_at"]);
                ?></span>
            </div>
        </div>
        <div class="extra content">
            <a>
                <i class="mail icon"></i>
                <?=$usuario["email"]?>
            </a>
        </div>
    </div>
    <?php if($soy_yo){ ?>
    <script type="text/javascript">
    var inputFile = document.getElementById("archivo_imagen");
    function readFile() {
        if (this.files && this.files[0]) {
            var FR = new FileReader();
            FR.onload = function(e) {
                document.getElementById("imagenActual").src = e.target.result;
                cambiarImagen(e.target.result);
            };
            FR.readAsDataURL(this.files[0]);
        }
    }
    inputFile.addEventListener("change", readFile, false);
    
    function cambiarImagen(img){
      $(".segment").dimmer("show");
        $.post( "/usuario/cambioImagen", { id: "<?=$usuario['id']?>", imagen:img  })
          .done(function( data ) {
              $(".segment").dimmer("hide");
          });
    }
    </script>
    <?php }?>
    <footer></footer>
</body>

</html>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />

  <title>Ingresar</title>
  <link type="text/css" rel="stylesheet" href="/public/semantic/semantic.css">
  <script src="/public/js/jquery.min.js"></script>
  <script type="text/javascript" src="/public/js/form_login.js"></script>
  <script type="text/javascript" src="/public/semantic/semantic.min.js"></script>
  
  <style type="text/css">
    body {
      background-color: #DADADA;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>
</head>
<body>

<div class="ui middle aligned center aligned grid">
  <div class="column">
   <h1 class="ui teal header">
      <img src="/public/media/logo_pdu.png" class="image">
      <div class="content">
       Ingresar
      </div>
    </h1>
    <?php if(isset($error)){?>
      <div class="ui pointing below red basic label">
        <?=$error?>
      </div>
    <?php }?>
    <form class="ui large form" action="/Usuario/signIn" method="post">
      <div class="ui stacked segment">
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="usuario" placeholder="Usuario">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="password" placeholder="Contraseña">
          </div>
        </div>
        <div class="ui fluid large teal submit button">Login</div>
      </div>
      <div class="ui error message"></div>
    </form>
  </div>
</div>

</body>

</html>

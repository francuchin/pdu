<div class="ui top fixed menu b" id="menuPrincipal">
    <a class="item" href="/">
        <img src="/public/media/logo_pdu.png">
    </a>
    <?php if(isset($indice) && $indice = true){ ?>
    <a href="javascript:alternar();" class="item" id="boton_alternar">Mostrar Listado</a>
    <?php }?>
        <div class="ui search item">
          <div class="ui left icon input">
            <input class="prompt" placeholder="Buscar Lugar" type="text">
            <i class="search icon"></i>
          </div>
        </div>
    <?php if(Session::is_admin()){ ?>
    <div class="right menu">
        <div class="ui dropdown item">
            Administrador <i class="dropdown icon"></i>
            <div class="menu">
                <a class="item" href="/lugar/nuevo">Agregar Lugar</a>
                <a class="item" href="/usuario/registro">Nuevo Administrador</a>
                <a class="item"href="/lugar/categorias">Gestionar Categorias</a>
                <!--
                <div class="item" onclick="verListadoCategoria()">Eliminar Categoria</div>
                <a class="item">Modificar Lugar</a>
                <a class="item">Borrar Lugar</a>
                -->
            </div>
        </div>
        <a class="item"><?=Session::getValue('U_NAME')?></a>
        <a class="item" href="/usuario/destroySession">Salir</a>
        
    </div>
    <?php }?>
</div>
<script type="text/javascript">
<?php if(Session::is_admin()){?>
    $('.ui.dropdown').dropdown();
<?php }?>
$('.ui.search').search({
    apiSettings: {
        url: '<?=URL?>lugar/busquedaWeb/{query}'
        },
    fields: {
      results : 'items',
      title   : 'nombre',
      url     : 'url'
    },
    minCharacters : 1,
    error : {
      source      : 'Cannot search. No source used',
      noResults   : 'No hay resultados',
      logging     : 'Error in debug logging, exiting.',
      noTemplate  : 'A valid template name was not specified.',
      serverError : 'Hay inconvenientes en el servidor',
      maxResults  : 'Results must be an array to use maxResults setting',
      method      : 'The method you called is not defined.'
    }
});
</script>
<script type="text/javascript" src="/public/js/menu.js"></script>
<!DOCTYPE html>
<html>
<head>
  <title>Pdu </title>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" >
  <link rel="stylesheet" href="/public/semantic/semantic.css" type="text/css" />
  <link rel="stylesheet" href="/public/css/estilos.css" type="text/css" />
  <link rel="stylesheet" href="/public/css/index.css" type="text/css" />
  <script type="text/javascript" src="/public/js/jquery.min.js"></script>
  <script type="text/javascript" src="/public/semantic/semantic.min.js"></script>
  <script type="text/javascript" src="/public/js/index.js"></script>
  <script type="text/javascript">
    function cargando(){
      $('.carga.segment').dimmer('show');
    }
    function cargado(){
      $('.carga.segment').dimmer('hide');
      $('.carga.segment').fadeOut();
    }
  </script>
</head>
<body>
  <?php if(isset($fondo["imagen"])){ ?>
        <img id="imagen_fondo" src="<?=$fondo['imagen']?>" style="display : none;" />
  <?php } ?>
  <?php require './views/menu.php'?>
  <div class="segments" id="segmentoIndice">
    <div class="portada">
      <img id="imagenPortada" src="http://media2.trover.com/T/553a4cb78e7cb24036001744/fixedw_large_4x.jpg"></img>
      <div class="texto_portada">
        <div class="contenido_texto">
          <h1 onclick="javascript:alternar();" >Paysandú</h1>
        </div>
      </div>
      <div class="descargarApp">
          <img class="celular" src="/public/media/celular.png"></img>
          <a class="free" href="#">
            <i class="certificate icon"><span>¡FREE!</span></i>
          </a>
          <a href="#">
            <img class="btnGetApp" src="/public/media/android-play-store.svg"></img>
          </a>
      </div>
    </div>
  </div>
  <div class="ui container">
    <div class="segments" id="segmentoLugares" style="display:none;">
    <?php if(isset($lugares)){ ?>
    <div class="ui carga segment">
      <div class="ui inverted dimmer">
        <div class="ui text loader">Cargando...</div>
      </div>
    </div>
    <script>cargando();</script>
    <div class="ui lugares segment items">
      <?php 
      $listado = $lugares->model->getidlist();
      if(isset($listado))
      foreach($listado as $id ){ 
      $articulo = $lugares->model->get($id["id"]); 
      $imagen = $lugares->model->getrandomimage($id["id"]);
      ?>
      <div class="ui lugar item">
        <div class="ui small image miniImagen">
          <?php if(isset($imagen)){ ?>
            <img src="<?=$imagen?>">
          <?php }else{ ?>
            <img src="/public/media/logo_pdu.png">
          <?php } ?>
        </div>
        <div class="middle aligned content">
          <div class="header">
            <?=$articulo["nombre"]?>
          </div>
          <div class="description">
            <?=substr(strip_tags($articulo["articulo"]), 0, 200);?>...
          </div>
          <div class="extra">
            <div class="ui right floated button" onclick="javascript:location.href='/lugar/<?=$id["id"]?>';">
              ver <i class="chevron circle right icon"></i>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>
    <?php } ?>
    </div>
  </div>
  <script type="text/javascript">
  var portadas = [
    "https://static.panoramio.com.storage.googleapis.com/photos/1920x1280/65473297.jpg", 
    "http://4.bp.blogspot.com/-ZJin2OJ8FcQ/UupaWDyW50I/AAAAAAAAGF0/vfcvceDdBEA/s1600/DSC06548.JPG",
    "http://3.bp.blogspot.com/-vOYOX09tZp0/VF0dxCUejHI/AAAAAAAAvCM/qmi1PwrP5Fs/s1600/20140509_104043.JPG",
    "http://4.bp.blogspot.com/-w8mjlxbFYuE/VYhoIDml-BI/AAAAAAAAGN8/q0Hj62ggFQY/s1600/P1050114.JPG",
    "/imagenes/efcc6aac918e70e492fa19cfde6cf847.png",
    "http://media2.trover.com/T/553a4cb78e7cb24036001744/fixedw_large_4x.jpg"
    ];
  var indiceImagen = 0;
  setInterval(function() {
   $('#imagenPortada').fadeOut(300, function(){
     indiceImagen === (portadas.length - 1)? indiceImagen = 0 : indiceImagen++;
      $(this).attr('src',portadas[indiceImagen]).bind('onreadystatechange load', function(){
         if (this.complete) $(this).fadeIn(300);
      });
   });
  }, 10000);
  cargado();
  </script>
  <footer></footer>
</body>

</html>
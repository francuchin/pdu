<!DOCTYPE html>
<html>

<head>
    <title><?=$lugar["nombre"]?></title>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <link type="text/css" rel="stylesheet" href="/public/semantic/semantic.css" />
    <link rel="stylesheet" href="/public/css/estilos.css" type="text/css" />
    <script type="text/javascript" src="/public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/public/semantic/semantic.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?=MAP_API_KEY?>" type="text/javascript"></script>
    <style type="text/css">
        .segment{
            background-color: rgba(255,255,255,0.9)!important;
        }
        h2,h4{
            color:white!important;
            text-shadow: 1px 1px 4px #fff;
        }
        h2 > .content,
        h4 {
            text-shadow: 1px 1px 4px #000;
        }
        h2.ui.header .sub.header{
            font-size:0.9rem;
            line-height:auto;
            text-transform: inherit;
        }
        .vistaTabla{
            display: inline-block;
            border-radius: 3px;
            border: 1px solid rgba(200,200,200,.7);
            background-color: rgb(250,250,250);
            margin: 5px;
            padding: 5px;
        }
    </style>
</head>
<body onload="initMap();">
    <?=isset($lugar["imagenes"]) ?  "<img id='imagen_fondo' src='".$lugar['imagenes'][rand(0,sizeof($lugar['imagenes'])-1)]['imagen']."'></img>" : "" ?>
    <?php require './views/menu.php'?>
    <div class="ui container">
    <div class="ui segment">
    <?php if(Session::is_admin()){?>
    <div id="admin">
        <div class="ui labeled button" onclick="javascript:location.href ='/lugar/editar/<?=$lugar["id"]?>';">
            <div class="ui blue button">
                <i class="edit icon"></i>
            </div>
            <a class="ui basic blue left pointing label">Editar</a>
        </div>
        <div class="ui labeled button" onclick="javascript:location.href ='/lugar/creartabla/<?=$lugar["id"]?>';">
            <div class="ui green button">
                <i class="table icon"></i>
            </div>
            <a class="ui basic green left pointing label">Agregar Nueva Tabla</a>
        </div>
         <div class="ui labeled button" onclick="javascript:verListas();">
            <div class="ui green button">
                <i class="table icon"></i>
            </div>
            <a class="ui basic green left pointing label">Modificar Categorias</a>
        </div>
        <div class="ui labeled button" onclick="javascript:eliminar();">
            <div class="ui red button">
                <i class="erase icon"></i>
            </div>
            <a class="ui basic red left pointing label">Eliminar</a>
        </div>
    </div>
    <div class="ui divider"></div>
    <?php } ?>
    <h2 class="ui header" id="titulo">
        <div class="content">
        <?=$lugar["nombre"]?>
        </div>
        <?php if(isset($lugar["calificacion"])){
            if($lugar["calificacion"] == 0 ){ ?> 
            <div class="sub header">Sin calificar <i class="empty star icon"></i></div>
        <?php } else{ ?>
            <div class="sub header">Calificacion de los usuarios: <?=round($lugar["calificacion"],1)?> <i class="star icon"></i></div>
        <?php } } ?>
    </h2>
    <div class="ver-articulo">
        <?=str_replace("\n", "<br>", $lugar["articulo"])?>
    </div>
    <div id="map"></div>
    <h4 class="ui horizontal divider header">
          <i class="photo icon"></i>
          Galeria
    </h4> 
    <?php if(Session::is_admin()){?>
    <div>
        <div class="ui labeled button" onclick="javascript:location.href ='/lugar/editarGaleria/<?=$lugar["id"]?>';">
            <div class="ui blue button">
                <i class="edit icon"></i>
            </div>
            <a class="ui basic blue left pointing label">Editar Galeria</a>
        </div>
    </div>
    <div class="ui divider"></div>
    <?php } ?>  
    <?php if(isset($lugar["imagenes"])){ ?>
        <div class="listadoImagenes">
            <?php foreach($lugar["imagenes"] as $imagen){ ?>
            <div class="previa_imagen">
                <img  src="<?=$imagen["imagen"]?>" style="display:inline-block;">
                <div class="btn_imagen">
                    <i class="zoom icon"></i>
                </div>
            </div>
            <?php } ?>
        </div>
    <?php } ?>
    <?php if(isset($lugar['tablas'])){?>
    <h4 class="ui horizontal divider header">
          <i class="plus icon"></i>
          Mas Datos
    </h4>
        <?php foreach($lugar['tablas'] as $tabla){?>
        <div class="vistaTabla" id="tabla_<?=$tabla['id']?>">
            <?php if(Session::is_admin()){?>
            <div class="mini ui right floated button" onclick="javascript:eliminarTabla(<?=$tabla['id']?>);">
                <i class="icon remove"></i>
            </div>
            <a class="mini ui right floated button" href="/lugar/creartabla/<?=$lugar['id']?>/<?=$tabla['id']?>">
                <i class="icon write"></i>
            </a>
            <?php } ?>
        <h2 class="ui header">
            <div class="content">
            <?=$tabla["nombre"]?>
            </div>

        </h2>

            <table class="ui collapsing celled table">
                <thead>
                    <tr>
                        <?php foreach($tabla['titulos'] as $titulo){?>
                            <th><?=$titulo?></th>
                        <?php }?>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($tabla['filas'] as $fila){?>
                    <tr>
                        <?php foreach($fila as $dato){?>
                            <td><?=$dato?></td>
                        <?php }?>
                    </tr>
                <?php }?>
                </tbody>
            </table>
        </div>
        <?php }?>
    <?php }?>
    <h4 class="ui horizontal divider header">
          <i class="comments icon"></i>
          Comentarios
    </h4> 
    <div class="ui comments">
        <?php 
        if(isset($comentarios)){ 
        foreach($comentarios as $comentario){ 
        ?>    
          <div class="comment">
            <a class="avatar">
              <img src="/public/media/logo_pdu.png" onload="this.onload = null; return getImagen(this, '<?=$comentario['user_email']?>');" >
            </a>
            <div class="content">
              <a class="author"><?=$comentario['user_name']?></a>
              <div class="metadata">
                  
                <div class="date"><?php 
                
                $timeZone = NULL; 
                $timeAgo = new TimeAgo($timeZone, 'es');
                echo $comentario['user_email']." - ".$timeAgo->inWords($comentario['created_at']);
                ?></div>
                <div class="rating">
                    <?php if(intval($comentario['puntuacion']) == 0){?>
                        <i class="empty star icon"></i>
                    <?php }else
                        for($estrellas = 0; $estrellas<intval($comentario['puntuacion']); $estrellas++){?>
                        <i class="star icon"></i>
                    <?php } ?>
                </div>
              </div>
              <div class="text">
                <?=$comentario['comentario']?>
              </div>
            </div>
          </div>
        <?php }
        }else{ ?>
        <div class="comment">
            <div class="content">
                <div class="text">
                    No hay comentarios de este lugar todavia
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="ui divider"></div>
        <div class="field">
            Puntuacion: <div class="ui huge rating" data-rating="5" data-max-rating="5" onrating></div>
        </div>
          <form class="ui form" method="post" action="/lugar/comentar">
            <input type="text" name="id_lugar" value="<?=$lugar['id']?>" hidden/>
            <input type="text" name="puntuacion" value="5" id="puntuacionComentario" hidden/>
            <div class="field">
                <label for="nombre_comentario">
                    <input type="text" id ="nombre_comentario" name="nombre" value="" placeholder="Nombre" require />
                </label>
            </div>
            <div class="field">
                <label for="email_comentario">
                    <input type="email" id ="email_comentario" name="email" value="" placeholder="Email" require/>
                </label>
            </div>
            <div class="field">
              <textarea name="comentario" require></textarea>
            </div>
            <button class="ui primary submit labeled icon button">
              <i class="icon comment"></i> Danos tu opinion
            </button>
          </form>
        </div>
    </div>
</div>
<footer></footer>
<?php if(Session::is_admin()){?>
<div class="edicionCategorias" id="edicionCategorias">
    <div class="listasCategoria">
        <div>
            <i style="cursor:pointer;" class="remove icon" onclick="ocultasListas()"></i>
        </div>
        <div id="listadoCategoriasActual">
            <ol></ol>
        </div>
        <div id="listadoCategoriasCompleto">
            <ol></ol>
        </div>
    </div>
</div>
<div class="ui basic modal borrarLugar">
    <i class="close icon"></i>
    <div class="header">
        Eliminar registro del lugar
    </div>
    <div class="image content">
        <div class="image">
            <i class="archive icon"></i>
        </div>
        <div class="description">
            <p>Esta accion no puede deshacerse, esta seguro?</p>
        </div>
    </div>
    <div class="actions">
        <div class="ui red basic inverted button cancel">
            <i class="remove icon"></i> No
        </div>
        <div class="ui green basic inverted button ok">
            <i class="checkmark icon"></i> Si
        </div>
    </div>
</div>
<div class="ui basic modal borrarTabla">
    <i class="close icon"></i>
    <div class="header">
        Eliminar registro de la tabla
    </div>
    <div class="image content">
        <div class="image">
            <i class="archive icon"></i>
        </div>
        <div class="description">
            <p>Esta accion no puede deshacerse, esta seguro?</p>
        </div>
    </div>
    <div class="actions">
        <div class="ui red basic inverted button cancel">
            <i class="remove icon"></i> No
        </div>
        <div class="ui green basic inverted button ok">
            <i class="checkmark icon"></i> Si
        </div>
    </div>
</div>
<?php } ?>
    <script type="text/javascript">
    <?php if(Session::is_admin()){?>
    var listadoCategoriasActual = document.getElementById('listadoCategoriasActual');
    var listadoCategoriasCompleto = document.getElementById('listadoCategoriasCompleto');
    var edicionCategorias = document.getElementById('edicionCategorias');
    
    function verListas(){
        listarCategorias();
        edicionCategorias.style.transform = "translateX(0)";
    }
    function ocultasListas(){
        edicionCategorias.removeAttribute('style');
    }
    var categorias = [];
    function containsObject(obj, list) {
        var x;
        for (x in list) {
            if (list.hasOwnProperty(x) && list[x] === obj) {
                return true;
            }
        }
        return false;
    }
    function listarCategorias(){
        var listaA =listadoCategoriasActual.getElementsByTagName('ol')[0];
        listaA.innerHTML="";
        var listaB = listadoCategoriasCompleto.getElementsByTagName('ol')[0];
        listaB.innerHTML="";
        
        var xhr = new XMLHttpRequest();
        xhr.addEventListener('load',function(){
            var cat = JSON.parse(xhr.response);
            var x = 0;
            categorias = [];
            if(!!cat)
            while(true){
                if(!cat[x]) break;
                var nombreCat =cat[x].nombre;
                categorias.push(nombreCat);
                var nodo = document.createElement('li');
                nodo.innerHTML=nombreCat;
                nodo.addEventListener('click', function(){
                   quitarCategoria(this.innerHTML);
                });
                listaA.appendChild(nodo);
                x++;
            }
        });
        xhr.open('GET','<?=URL?>lugar/listarCategorias/<?=$lugar['id']?>' );
        xhr.setRequestHeader('Accept', 'application/json');
        xhr.send();
        
        var xhr2 = new XMLHttpRequest();
        xhr2.addEventListener('load',function(){
            var cat = JSON.parse(xhr2.response);
            var x = 0;
            if(!!cat)
            while(true){
                if(!cat[x]) break
                var nombreCat = cat[x].nombre;
                 if(!containsObject(nombreCat,categorias)){
                    var nodo = document.createElement('li');
                    nodo.innerHTML=nombreCat;
                    nodo.addEventListener('click', function(){
                       agregarCategoria(this.innerHTML);
                    });
                    listaB.appendChild(nodo);
                }
                x++;
            }
        });
        xhr2.open('GET','<?=URL?>lugar/listarCategorias/' );
        xhr2.setRequestHeader('Accept', 'application/json');
        xhr2.send();
        
    }
    function agregarCategoria(nombre){
        console.log('xd');
        $.post( "<?=URL?>lugar/setCategoria/<?=$lugar['id']?>", { categoria:nombre})
        .done(function( data ) {
            console.log(data);
            listarCategorias();
        })
        .fail(function(data){
            console.log(data);
        });
    }
    function quitarCategoria(nombre){
        $.post( "<?=URL?>lugar/unsetCategoria/<?=$lugar['id']?>", { categoria:nombre})
        .done(function( data ) {
            listarCategorias();
        });
    }
    <?php } ?>
    
    function getImagen(perfil, mail){
      if(mail.split('@')[1]){
            var usuario = mail.split('@')[0];
            var dominio = mail.split('@')[1];
            if(dominio.split('.')[0]=="gmail"){
                var xhr = new XMLHttpRequest();
                xhr.addEventListener('load',function(){
                    var imgsss = JSON.parse(xhr.response);
                    perfil.setAttribute('src', imgsss.entry.gphoto$thumbnail.$t); 
                });
                xhr.addEventListener('error',function(){
                    perfil.setAttribute('src', "/public/media/logo_pdu.png");
                });
                xhr.open('GET','https://picasaweb.google.com/data/entry/api/user/'+ usuario +'?alt=json' );
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.send();
            }else{
                perfil.setAttribute('src', "/public/media/logo_pdu.png");
            }
       }else{
            perfil.setAttribute('src', "/public/media/logo_pdu.png");
       }
    }
        $('.ui.rating').rating({
            onRate : function(value) {
                document.getElementById('puntuacionComentario').value=value;
            }
        });
        var map = null;
        var ubicacion = {
            lat: <?=$lugar["latitud"]?>,
            lng: <?=$lugar["longitud"]?>
        };
        function CenterControl(controlDiv, map) {
            // Settear clase para control border.
            var controlUI = document.createElement('div');
            controlUI.classList.add("controlUI");
            controlDiv.appendChild(controlUI);
            var icono = document.createElement('i');
            icono.classList.add("crosshairs");
            icono.classList.add("icon");
            icono.title = "Centrar";
            controlUI.appendChild(icono);
            // agregar evento al control
            controlUI.addEventListener('click', function() {
                map.setCenter(ubicacion);
                map.setZoom(16);
            });
        }
        
        function initMap() {
            
            var map = new google.maps.Map(document.getElementById('map'), {
                center: ubicacion,
                mapTypeId: google.maps.MapTypeId.HYBRID,
                draggable: false,
                zoomControl: true,
                scrollwheel: false,
                scaleControl: false,
                zoom: 16,
                disableDefaultUI: true
            });
            var geocoder = new google.maps.Geocoder();
       
            var marker = new google.maps.Marker({
                map: map,
                position: ubicacion,
                title: '<?=$lugar["nombre"]?>'
            });
            
            /*--sacar lugares que vienen en google maps--*/
            var noPoi = [
            {
                featureType: "poi",
                stylers: [
                  { visibility: "off" }
                ]   
              }
            ];
            map.setOptions({styles: noPoi});
            /*----*/
            
            geocoder.geocode({'location':ubicacion}, function(results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                  if (results[0]) {
                    var p = document.createElement('div');
                    p.classList.add("sub");
                    p.classList.add("header");
                    p.appendChild(document.createTextNode('Ubicacion: ' + results[0]['formatted_address']));
                    document.getElementById('titulo').appendChild(p);
                   // console.log(results[0]['formatted_address']);
                   var label = document.createElement('div');
                   label.className = "labelMapDireccion";
                   label.innerHTML =  results[0]['formatted_address'];
                   label.index = 1;
                   map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(label);
                   
                  } else {
                   console.log('No results found');
                  }
                } else {
                  console.log('Geocoder failed due to: ' + status);
                }
              });
            var contentString = '<div>' +
                '<div><h4 ><b><?=$lugar["nombre"]?></b></h4></div>'+
               /* '<div id="bodyContent">' +
                '<p></p>'+
                '<p>'+
                '<?=str_replace("\r", "",str_replace("\n","<br>",$lugar["articulo"]))?>'+
                '</p>'+
                <?php if (isset($lugar["imagenes"])) { ?>
                '<img class="ui small left floated image" src="<?=$lugar["imagenes"][0]["imagen"]?>">' +
                <?php } ?>
            '</div>' +*/
            '</div>';
            var infowindow = new google.maps.InfoWindow({
                content: contentString,
                maxWidth: 400
            });
            marker.addListener('click', function() {
                infowindow.open(map, marker);
            });
            var centerControlDiv = document.createElement('div');
            var centerControl = new CenterControl(centerControlDiv, map);
            centerControlDiv.index = 1;
            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(centerControlDiv);
        }
<?php if(Session::is_admin()){?>
        function eliminar(){
            $('.ui.basic.modal.borrarLugar').modal({
                onApprove : function() {
                    console.log("Eliminando...\n");
                    $.post( "/lugar/eliminar", { id:"<?=$lugar['id']?>"})
                      .done(function( data ) {
                          console.log(data);
                          if(data === true)
                          document.getElementById("admin").innerHTML = "<div class='ui divided red selection list'><a href='/' class='item'><div class='ui red tag label'>Borrado exitosamente</div> Este lugar ya no se encuentra en la base de datos, ir al inicio?</a></div>";
                      }).fail( function(xhr, textStatus, errorThrown) {
                            console.log("\nOCURRIO UN ERROR: \n");
                            console.log(xhr.responseText);
                        });
                }
              }).modal('show');
        }
        function eliminarTabla(id){
            $('.ui.basic.modal.borrarTabla').modal({
                onApprove : function() {
                    $.post( "/lugar/eliminarTabla/" + id)
                      .done(function( data ) {
                              var tabla = document.getElementById("tabla_"+id);
                              tabla.parentElement.removeChild(tabla);
                      }).fail( function(xhr, textStatus, errorThrown) {
                            console.log("\nOCURRIO UN ERROR: \n");
                            console.log(xhr.responseText);
                        });
                }
              }).modal('show');   
        }
<?php } ?>
        </script>
</body>
</html>
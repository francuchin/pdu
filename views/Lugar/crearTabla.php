<!DOCTYPE html>
<html>
<head>
    <title>Tabla - <?=$lugar["nombre"]?></title>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <link type="text/css" rel="stylesheet" href="/public/semantic/semantic.css" />
    <link rel="stylesheet" href="/public/css/estilos.css" type="text/css" />
    <script type="text/javascript" src="/public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/public/semantic/semantic.min.js"></script>
    <style type="text/css">
        .segment {
            background-color: rgba(255, 255, 255, 0.9)!important;
        }
        
        h2,
        h4 {
            color: white!important;
            text-shadow: 1px 1px 4px #fff;
        }
        
        h2> .content,
        h4 {
            text-shadow: 1px 1px 4px #000;
        }
        
        h2.ui.header .sub.header {
            font-size: 0.9rem;
            line-height: auto;
            text-transform: inherit;
        }
        #menuTablaContextual{
            position: absolute;
            left:0;
            top:0;
            opacity: 0;
            transition: opacity .1s;
            visibility: hidden;
            background-color: white;
            padding: 0;
            border: 1px solid rgba(0,0,0,.4);
            box-shadow: 0 4px 5px 0 rgba(0,0,0,.14),0 1px 10px 0 rgba(0,0,0,.12),0 2px 4px -1px rgba(0,0,0,.2);
            border-radius: 2px;
        }
        #menuTablaContextual > div {
            cursor: pointer;
            padding: 5px;
            padding-left:10x;
            padding-right:10px;
            border-top: solid 1px rgba(200,200,200,.9);
            transition: background .3s;
        }
        #menuTablaContextual > div:first-child{
            border-top: none;
        }
        #menuTablaContextual > div:hover{
            background-color: rgba(200,200,200,.5);
        }
        th > div:hover,
        td > div:hover{
            cursor:pointer;
        }
    </style>
</head>
<body >
    <?=isset($lugar["imagenes"]) ?  "<img id='imagen_fondo' src='".$lugar['imagenes'][rand(0,sizeof($lugar['imagenes'])-1)]['imagen']."'></img>" : "" ?>
    <?php require './views/menu.php'?>
    <div class="ui container">
        <div id="segmentoprincipal" class="ui segment">
        <h2 class="ui header" id="titulo">
            <div class="content">
            <?=$lugar["nombre"]?> - <?=(isset($tabla))? $tabla["nombre"] : "Nueva Tabla" ?>
            </div>
        </h2>
            <div class="ui left corner labeled input">
                <input placeholder="Nombre de la tabla" type="text" id="inputNombreTabla">
                <div class="ui left corner label">
                    <i class="tag icon"></i>
                </div>
            </div>
            <div class="ui button" onclick="agregarColumna();">
                <i class="icon plus"></i> Agregar Columna
            </div>
           <div class="ui button" onclick="agregarFila();">
                <i class="icon plus"></i> Agregar Fila
            </div>
           <div class="ui button" onclick="enviarTabla();">
                <i class="icon send"></i> Crear Nueva Tabla
            </div>
            <?php if(isset($tabla)){ ?>
             <div class="ui button" onclick="modificarTabla();">
                <i class="icon send"></i> Modificar Tabla
            </div>
            <?php } ?>
            <table class="ui celled table">
                <thead>
                    <tr>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    </div>
    <div id="menuTablaContextual" class"menuTablaContextual">
        <div >Editar</div>
        <div>Borrar Fila</div>
        <div>Borrar Columna</div>
    </div>
    <script type="text/javascript">
        var cantidadColumnas = 0;
        var cantidadFilas = 0;
        var tabla = document.getElementsByTagName('table')[0];
        var headT = tabla.getElementsByTagName('thead')[0];
        var bodyT = tabla.getElementsByTagName('tbody')[0];
        var menuTablaContextual = document.getElementById('menuTablaContextual');
        document.body.addEventListener('click',function(e){quitarMenuTabla(e);},false);
        tabla.addEventListener('contextmenu', function(e){e.preventDefault();},false);
        function transitionendEvent(){menuTablaContextual.style.visibility = "hidden";}
        function quitarMenuTabla(e){
            if(e.target !== menuTablaContextual){
                menuTablaContextual.addEventListener("transitionend", transitionendEvent, false);
                if(menuTablaContextual){
                    menuTablaContextual.style.opacity = 0;
                }
            }
        }
        function verMenuTabla(e){
            e.preventDefault();
            menuTablaContextual.removeEventListener("transitionend", transitionendEvent, false);
            if(menuTablaContextual){
                var editar = document.createElement('div');
                var borrarColumna = document.createElement('div');
                editar.innerHTML = "<i class='write icon'></i>editar";
                borrarColumna.innerHTML = "<i class='arrow circle outline down icon'></i> borrar columna";
                editar.addEventListener('click', function(){editText(e.target)},false);
                borrarColumna.addEventListener('click', function(){eliminarColumna(e.target)},false);
                menuTablaContextual.innerHTML = '';
                menuTablaContextual.appendChild(editar);
                menuTablaContextual.appendChild(borrarColumna);
                if(e.target.parentElement.tagName == "TD"){
                    var borrarFila = document.createElement('div');
                    borrarFila.innerHTML = "<i class='arrow circle outline right icon'></i>borrar fila";
                    borrarFila.addEventListener('click', function(){eliminarFila(e.target)},false);
                    menuTablaContextual.appendChild(borrarFila);
                }
                menuTablaContextual.style.transform = "translate(" +e.clientX+"px, "+e.clientY+"px)";
                menuTablaContextual.style.visibility = "visible";
                menuTablaContextual.style.opacity = 1;
            }
        }
        String.prototype.isEmpty = function() {
            return (this.length === 0 || !this.trim());
        };
        function setTexto(e, elemento){
            var tecla = (document.all) ? e.keyCode : e.which;
            if (tecla == 13) { 
                e.preventDefault();
                if(!elemento.value.isEmpty()){
                    var valor = elemento.value;
                    var contenidoCelda = document.createElement('div');
                    contenidoCelda.addEventListener('contextmenu', function(e){verMenuTabla(e);},false);
                    var abuelo = elemento.parentElement.parentElement;
                    contenidoCelda.innerHTML = valor;
                    abuelo.innerHTML = "";
                    abuelo.appendChild(contenidoCelda);
                }
            }
        }
        function eliminarColumna(elemento){
            var i = 0;
            child = elemento.parentElement;
            while( (child = child.previousSibling) != null ) i++;
            if(elemento.parentElement.tagName == "TD" || elemento.parentElement.tagName == "td") i++;
            cabeceras = headT.getElementsByTagName('tr')[0].getElementsByTagName('th');
            headT.getElementsByTagName('tr')[0].removeChild(cabeceras[i-1]); // borro la cabecera
            rows = bodyT.getElementsByTagName('tr');
            for(var j=0; j < rows.length ; j++){
                var celdas = rows[j].getElementsByTagName('td');
                rows[j].removeChild(celdas[i-1]);
            }
             for(j = rows.length - 1; j >= 0 ; j--){
                if(!rows[j].getElementsByTagName('td')[0]){
                    bodyT.removeChild(rows[j]);
                    cantidadFilas--;
                }
            }
            cantidadColumnas--;
            
        }
        function eliminarFila(elemento){
            bodyT.removeChild(elemento.parentElement.parentElement);
            cantidadFilas--;
        }
        function editText(elemento){
            target = elemento.parentElement;
            var inputText = document.createElement('div'); // inputTextF
                inputText.classList.add('ui');
                inputText.classList.add('input');
            var iText = document.createElement('input');
                iText.setAttribute('type', 'text');
                inputText.appendChild(iText);
                iText.setAttribute('value', elemento.innerHTML);
            iText.addEventListener('keyup', function(e){setTexto(e, this);});
            target.innerHTML = "";
            target.appendChild(inputText);
        }
        function agregarColumna(nombreColumna) {
            cantidadColumnas++;
            var tituloCol = document.createElement('th');
            var inputTextC = document.createElement('div'); //  inputTextC
            inputTextC.classList.add('ui');
            inputTextC.classList.add('input');
            var iTextC = document.createElement('input');
            iTextC.setAttribute('type', 'text');
            if(!!nombreColumna)
                iTextC.setAttribute('value', nombreColumna);
            else
                iTextC.setAttribute('value', '');
            iTextC.setAttribute('placeholder', 'Nombre Columna');
            inputTextC.appendChild(iTextC);
            iTextC.addEventListener('keyup', function(e){setTexto(e, this);});
            tituloCol.appendChild(inputTextC);
            headT.getElementsByTagName('tr')[0].appendChild(tituloCol);
            var filas = bodyT.getElementsByTagName('tr');
            Array.prototype.forEach.call(filas, function(fila, index){
                var inputTextF = document.createElement('div'); // inputTextF
                    inputTextF.classList.add('ui');
                    inputTextF.classList.add('input');
                var iTextF = document.createElement('input');
                    iTextF.setAttribute('type', 'text');
                    iTextF.setAttribute('placeholder', 'Valor');
                inputTextF.appendChild(iTextF);
                iTextF.addEventListener('keyup', function(e){setTexto(e, this);});
                var nodoFila = document.createElement('td');
                nodoFila.appendChild(inputTextF);
                fila.appendChild(nodoFila);
            });
        }
        function agregarFila(valoresInsertar) {
            if(cantidadColumnas > 0){
                cantidadFilas++;
                var nuevaFila = document.createElement('tr');
                for(var j = 0 ; j < cantidadColumnas; j++){
                    var inputTextF = document.createElement('div'); // inputTextF
                        inputTextF.classList.add('ui');
                        inputTextF.classList.add('input');
                    var iTextF = document.createElement('input');
                        iTextF.setAttribute('type', 'text');
                        iTextF.setAttribute('placeholder', 'Valor');
                        if(!!valoresInsertar && !!valoresInsertar[j])
                            iTextF.setAttribute('value', valoresInsertar[j]);
                    inputTextF.appendChild(iTextF);
                    iTextF.addEventListener('keyup', function(e){setTexto(e, this);});
                    var nodoFila = document.createElement('td');
                    nodoFila.appendChild(inputTextF);
                    nuevaFila.appendChild(nodoFila);
                }
                bodyT.appendChild(nuevaFila);
            }
        }
        function enviarTabla(){
            if(document.getElementById('inputNombreTabla').value.isEmpty()){
              document.getElementById('inputNombreTabla').parentElement.classList.add('error');
              document.getElementById('inputNombreTabla').focus();
              return false;
            }
            var inputs = tabla.getElementsByTagName('input');
            if(!!inputs[0]){
                inputs[0].parentElement.classList.add('error');
                inputs[0].focus();
                return false;
            }   // ver que todos los campos tengan valores aceptados
            var titulos = new Object();
            var filas = new Object();
            var cabeceras = headT.getElementsByTagName('tr')[0].getElementsByTagName('th');
            if(!cabeceras[0]){
                console.log("Agregar al menos una columna");
                return false;
            }
            Array.prototype.forEach.call(cabeceras, function(cabecera, index){
                titulos[index] = cabecera.getElementsByTagName('div')[0].innerHTML;
                return false;
            });
            var rows = bodyT.getElementsByTagName('tr');
            Array.prototype.forEach.call(rows, function(row, index){
                var campos = row.getElementsByTagName('td');
                filas[index] = new Object();
                Array.prototype.forEach.call(campos, function(campo, index2){
                    filas[index][index2] = campo.getElementsByTagName('div')[0].innerHTML;
                });
            });
            var respuesta = new Object();
            respuesta['nombre'] = document.getElementById('inputNombreTabla').value;
            respuesta['lugar'] = <?=$lugar['id']?>;
            respuesta['titulos'] = titulos;
            respuesta['filas'] = filas;
            var json = new Object();
            json['tabla'] = respuesta;
            $('#segmentoprincipal').dimmer('show');
            xhr = new XMLHttpRequest();
            xhr.addEventListener('load', function(){
                window.location.assign("<?=URL.'lugar/'.$lugar['id']?>");
            });
            xhr.open('POST', '<?=URL?>lugar/creartabla');
            xhr.setRequestHeader("Content-type", "application/json");
            xhr.send(JSON.stringify(json));
        }
        
        function confirmarCampo(elemento) {
                var valor = elemento.value;
                var contenidoCelda = document.createElement('div');
                contenidoCelda.addEventListener('contextmenu', function(e){verMenuTabla(e);},false);
                var abuelo = elemento.parentElement.parentElement;
                contenidoCelda.innerHTML = valor;
                abuelo.innerHTML = "";
                abuelo.appendChild(contenidoCelda);
        }
        function confirmarTodosLosCampos(){
            var cabeceras = headT.getElementsByTagName('tr')[0].getElementsByTagName('th');
            Array.prototype.forEach.call(cabeceras, function(cabecera, index){
                 confirmarCampo(cabecera.getElementsByTagName('input')[0]);
            });
            var rows = bodyT.getElementsByTagName('tr');
            Array.prototype.forEach.call(rows, function(row, index){
                var campos = row.getElementsByTagName('td');
                Array.prototype.forEach.call(campos, function(campo, index2){
                    confirmarCampo(campo.getElementsByTagName('input')[0]);
                });
            });      
        }
<?php if(isset($tabla)){ ?>
    function modificarTabla(){
            var inputs = tabla.getElementsByTagName('input');
            if(!!inputs[0]){
                inputs[0].parentElement.classList.add('error');
                inputs[0].focus();
                return false;
            }   // ver que todos los campos tengan valores aceptados
            var titulos = new Object();
            var filas = new Object();
            var cabeceras = headT.getElementsByTagName('tr')[0].getElementsByTagName('th');
            if(!cabeceras[0]){
                console.log("Agregar al menos una columna");
                return false;
            }
            Array.prototype.forEach.call(cabeceras, function(cabecera, index){
                titulos[index] = cabecera.getElementsByTagName('div')[0].innerHTML;
                return false;
            });
            var rows = bodyT.getElementsByTagName('tr');
            Array.prototype.forEach.call(rows, function(row, index){
                var campos = row.getElementsByTagName('td');
                filas[index] = new Object();
                Array.prototype.forEach.call(campos, function(campo, index2){
                    filas[index][index2] = campo.getElementsByTagName('div')[0].innerHTML;
                });
            });
            var respuesta = new Object();
            respuesta['nombre'] = "<?=$tabla['nombre']?>";
            if(!document.getElementById('inputNombreTabla').value.isEmpty())
                respuesta['nombre'] = document.getElementById('inputNombreTabla').value;
            respuesta['lugar'] = "<?=$lugar['id']?>";
            respuesta['id'] = "<?=$tabla['id']?>";
            respuesta['titulos'] = titulos;
            respuesta['filas'] = filas;
            var json = new Object();
            json['tabla'] = respuesta;
            $('#segmentoprincipal').dimmer('show');
            xhr = new XMLHttpRequest();
            xhr.addEventListener('load', function(){
               window.location.assign("<?=URL.'lugar/'.$lugar['id']?>");
            });
            xhr.open('POST', '<?=URL?>lugar/modificartabla');
            xhr.setRequestHeader("Content-type", "application/json");
            xhr.send(JSON.stringify(json));  
    }

    <?php foreach($tabla['titulos'] as $titulo){?>
        agregarColumna('<?=$titulo?>');
    <?php }?>
    <?php foreach($tabla['filas'] as $fila){ $primerValor = true; ?>
    agregarFila([<?php 
        foreach($fila as $dato){
            if($primerValor) 
                $primerValor = false; 
            else echo ","; 
            echo "'".$dato."'";
        }?>]);
    <?php }?>
    confirmarTodosLosCampos();
    <?php }?>
    </script>
</body>

</html>
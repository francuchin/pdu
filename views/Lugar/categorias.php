<!DOCTYPE html>
<html>
<head>
    <title>Categorias</title>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <link type="text/css" rel="stylesheet" href="/public/semantic/semantic.css" />
    <link rel="stylesheet" href="/public/css/estilos.css" type="text/css" />
    <script type="text/javascript" src="/public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/public/semantic/semantic.min.js"></script>
    <style type="text/css">
        body{
             background-color: rgb(210,210,210)!important;
        }
        .segment{
            background-color: rgba(255,255,255,0.9)!important;
        }
        h2,h4{
            color:white!important;
            text-shadow: 1px 1px 4px #fff;
        }
        h2 > .content,
        h4 {
            text-shadow: 1px 1px 4px #000;
        }
        .ui.avatar.image{
            border-radius: 0!important;
        }
    </style>
</head>
<body>
    <?php require './views/menu.php'?>
    <div class="ui container">
        <div class="ui segment">
            <h2 class="ui header" id="titulo">
                <div class="content">Categorias</div>
            </h2>
            <div class="ui middle aligned divided list">
                <?php 
                if(isset($categorias))
                foreach($categorias as $categoria){?>
                <div class="item">
                    <div class="right floated content">
                        <a class="ui red button" href="/lugar/webdeletecategoria/<?=$categoria['nombre']?>">Borrar</a>
                    </div>
                    <div class="right floated content">
                        <a class="ui button">Editar</a>
                    </div>
                    <img class="ui avatar image" src="<?=$categoria['imagen']?>">
                    <div class="content" style="text-transform: capitalize;"><?=$categoria['nombre']?></div>
                </div>
                <?php } ?>
                <form class="item" method="POST">
                    <label for="imagen">
                        <img class="ui avatar image" id="imagenPrevia" src="/public/media/mas.png">
                        <input id="imagen" type='file' accept="image/*" hidden required>
                        <input id="imagenInput" type="text" name="imagen" hidden/>
                    </label>
                    <div class="ui left input">
                        <input placeholder="Nueva Categoria" type="text" name="nombre" required>
                     </div>
                     <div class="right floated content">
                        <button class="ui button">OK</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<footer></footer>
<script type="text/javascript">
  function readFile() {
    if (this.files && this.files[0]) {
      var FR = new FileReader();
      FR.onload = function(e) {
        var nueva_imagen = document.getElementById('imagenPrevia');
        var input = document.getElementById('imagenInput');
        input.setAttribute('value', e.target.result)
        nueva_imagen.src = e.target.result;
      };
      FR.readAsDataURL(this.files[0]);
    }
  }
  document.getElementById("imagen").addEventListener("change", readFile, false);
</script>
</body>
</html>
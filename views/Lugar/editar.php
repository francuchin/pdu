<!DOCTYPE html>
  <html>
  <head>
    <title>Editar - <?=$lugar["nombre"]?></title>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <link type="text/css" rel="stylesheet" href="/public/semantic/semantic.css" />
    <link rel="stylesheet" href="/public/css/estilos.css" type="text/css" />
    <script type="text/javascript" src="/public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/public/semantic/semantic.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo MAP_API_KEY ?>" type="text/javascript"></script>
    <style type="text/css">
        .segment{
            background-color: rgba(255,255,255,0.9)!important;
        }
        h2{
            color:white!important;
            text-shadow: 1px 1px 4px #000;
        }
        h2 > .content{
            text-shadow: 1px 1px 4px #fff;
        }
    </style>
  </head>
  <body onLoad="load();">
    <?php if(isset($fondo["imagen"])){ ?>
        <img id="imagen_fondo" src="<?=$fondo['imagen']?>"></img>
    <?php } ?>
    <?php require './views/menu.php'?>
    <div class="ui container">
      <div class="ui segment">
         <div class="ui inverted dimmer">
                <div class="ui text loader">Actualizando Datos</div>
            </div>
      <form class="ui form" method="post" onsubmit="javascript:dimmerEnviando();">
          <input type="text" value="<?=$lugar["id"]?>" name="id" hidden/>
          <h2>Lugar</h2>
          <div class="field">
            <label>Nombre:
              <input type="text" name="nombre" value="<?=$lugar["nombre"]?>" required>
            </label>
          </div>
          <div class="field">
            <label for="">Ubicacion: </label>
            <input type="text" id="latitud" value="<?=$lugar["latitud"]?>" name="latitud" hidden/>
            <input type="text" id="longitud" value="<?=$lugar["longitud"]?>" name="longitud" hidden/>
            <div align="center" id="map"></div>
          </div>
          <h2>Articulo</h2>
          <div class="field">
            <label>Contenido:
              <textarea rows="23" name="articulo" required><?=$lugar["articulo"]?></textarea>
            </label>
          </div>
          <center>
            <button class="ui primary button" type="submit" id="submit">Confimar</button>
          </center>
          <br>
      </form>
      </div>
    </div>
      <script type="text/javascript">
        function dimmerEnviando(){
            $('.segment').dimmer('show');
        }
        
        /*-----MAP----*/
        var map = null;
        var infoWindow = null;
    
        function openInfoWindow(marker) {
          var markerLatLng = marker.getPosition();
          infoWindow.setContent([
            '<strong>La posicion del marcador es:</strong><br/>',
            markerLatLng.lat(),
            ', ',
            markerLatLng.lng(),
            '<br/>Arrástrame para actualizar la posición.'
          ].join(''));
          infoWindow.open(map, marker);
        }

        function load() {
          var coordenada1 = "<?=$lugar["latitud"]?>";
          var coordenada2 = "<?=$lugar["longitud"]?>";

          var map = new google.maps.Map(document.getElementById("map"), {
            center: new google.maps.LatLng(coordenada1, coordenada2),
            zoom: 14,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            mapTypeId: google.maps.MapTypeId.HYBRID
          });
          infoWindow = new google.maps.InfoWindow();

          var marker = new google.maps.Marker({
            position: new google.maps.LatLng(coordenada1, coordenada2),
            draggable: true,
            map: map,
            title: "Seleccione Ubicacion"
          });
          google.maps.event.addListener(marker, 'dragend', function() {
            //openInfoWindow(marker);
            posicion = marker.getPosition();
            document.getElementById("latitud").value = posicion.lat();
            document.getElementById("longitud").value = posicion.lng();
            console.log("Latitud: " + posicion.lat() + "\nLongitud: " + posicion.lng());
          });
          google.maps.event.addListener(marker, 'click', function() {
            openInfoWindow(marker);
          });
        }
      </script>

  </body>

  </html>
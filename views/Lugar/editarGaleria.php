<!DOCTYPE html>
<html>

<head>
    <title>Galeria - <?=$lugar["nombre"]?></title>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <link type="text/css" rel="stylesheet" href="/public/semantic/semantic.css" />
    <link rel="stylesheet" href="/public/css/estilos.css" type="text/css" />
    <script type="text/javascript" src="/public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/public/semantic/semantic.min.js"></script>
    <style type="text/css">
        .segment {
            background-color: rgba(255, 255, 255, 0.9)!important;
        }
        
        h2 {
            color: black!important;
            text-shadow: 1px 1px 2px #000;
        }
        
        h2 > .content {
            text-shadow: 1px 1px 2px #000;
        }
    </style>
</head>
<body>
    
    <?php if(isset($fondo["imagen"])){ ?>
    <img id="imagen_fondo" src="<?=$fondo['imagen']?>"></img>
    <?php } ?>
    <?php require './views/menu.php'?>
    <div class="ui container">
        <div class="ui segment">
            <div class="ui inverted dimmer">
                <div class="ui text loader">Actualizando Galeria</div>
            </div>
             <h2 class="ui header">
              <div class="content">Editando la galeria de imagenes</div>
            </h2>
            <div class="ui divider"></div>
            <form class="ui form" method="post" onsubmit="javascript:dimmerEnviando();">
                <input type="text" name="id" value="<?=$id?>" hidden/>
                <?php 
                    if(isset($imagenes)) {
                    foreach($imagenes as $img){
                ?>
                <label>
                <input type="checkbox" name="imagenes_borrar[]" value="<?=$img['id']?>" hidden/> 
                <div class="previa_imagen">
                    <img  src="<?=$img["imagen"]?>" style="display:inline-block;">
                </div>
                </label>
                <?php } ?>
                 <div class="ui divider"></div>
                <?php } ?>
         <label for="inputImagenes" class="ui basic button">
            <i class="add circle icon"></i><span>Agregar imagen</span>
            <input id="inputImagenes" type='file' accept="image/*" hidden>
         </label>
          <br>
         <div id="contenedorImagenes"></div>        
        <center>
            <button class="ui primary button" type="submit" id="submit">Confimar</button>
         </center>
            </form>
        </div>
    </div>
     <script type="text/javascript">
        function dimmerEnviando(){
            $('.segment').dimmer('show');
        }
        /*--- Imagenes---*/
        var id_imagen = 0;

        function readFile() {
          if (this.files && this.files[0]) {
            var FR = new FileReader();
            FR.onload = function(e) {
              var item_imagen = document.createElement('div');
              item_imagen.id = "item_imagen_" + id_imagen;
              item_imagen.classList.add("previa_imagen");

              var nueva_imagen = document.createElement('img');
              nueva_imagen.src = e.target.result;

              var input_imagen = document.createElement('input');
              input_imagen.name = "imagenes_agregar[]";
              input_imagen.value = e.target.result;
              input_imagen.hidden = true;

              var iconoX = document.createElement('i');
              iconoX.classList.add('remove');
              iconoX.classList.add('icon');
              iconoX.addEventListener('click', function() {
                $(this).parent().parent().remove();
              }, false);

              var btn_eliminar = document.createElement('div');
              btn_eliminar.classList.add("btn_imagen");
              btn_eliminar.appendChild(iconoX);
             
              item_imagen.appendChild(nueva_imagen);
              item_imagen.appendChild(input_imagen);
              item_imagen.appendChild(btn_eliminar);

              var ultimo_item = document.getElementById('contenedorImagenes').getElementsByTagName('div')[0];
              document.getElementById('contenedorImagenes').insertBefore(item_imagen, ultimo_item);

            };
            FR.readAsDataURL(this.files[0]);
          }
          id_imagen++;
        }
        document.getElementById("inputImagenes").addEventListener("change", readFile, false);
    </script>
</body>
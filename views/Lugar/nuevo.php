<!DOCTYPE html>
<html>
<head>
  <title>Nuevo Lugar Paysandú</title>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
  <link type="text/css" rel="stylesheet" href="/public/semantic/semantic.css" />
  <link rel="stylesheet" href="/public/css/estilos.css" type="text/css" />
  <script type="text/javascript" src="/public/js/jquery.min.js"></script>
  <script type="text/javascript" src="/public/js/nuevo_lugar.js"></script>
  <script type="text/javascript" src="/public/semantic/semantic.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo MAP_API_KEY ?>" type="text/javascript"></script>
</head>
<body onLoad="load();">
<?php require './views/menu.php'?>
<div class="ui container">
  <form class="ui form" method="post" action="/lugar/guardar">
    <div class="ui four top attached steps">
      <div id="paso1" class="active step">
        <i class="marker icon"></i>
        <div class="content">
          <div class="title">Lugar</div>
          <div class="description">Define el lugar de Paysandu que vas a ingresar</div>
        </div>
      </div>
      <div id="paso2" class="disabled step">
        <i class="browser icon"></i>
        <div class="content">
          <div class="title">Articulo</div>
          <div class="description">Edita un articulo con sus detalles relevantes</div>
        </div>
      </div>
      <div id="paso3" class="disabled step">
        <i class="camera retro icon"></i>
        <div class="content">
          <div class="title">Imagenes</div>
          <div class="description">Crea una galeria de imagenes del lugar</div>
        </div>
      </div>
      <div id="paso4" class="disabled step">
        <i class="checkmark icon"></i>
        <div class="content">
          <div class="title">Confirmar</div>
        </div>
      </div>
    </div>
    <div class="paso1">
      <h2>Lugar</h2>
      <div class="field">
        <label>Nombre:
          <input type="text" name="lugar-nombre" required>
        </label>
      </div>
      <div class="field">
        <label for="">Ubicacion: </label>
        <input type="text" id="lugar-ubicacion-latitud" value="-32.3099924" name="lugar-ubicacion-latitud" hidden/>
        <input type="text" id="lugar-ubicacion-longitud" value="-58.0603207" name="lugar-ubicacion-longitud" hidden/>
        <div align="center" id="map"></div>
      </div>
      <div class="ui right labeled icon button siguiente" onclick="javascript:verPaso('paso2');">
        <i class="right arrow icon"></i> Siguiente
      </div>
    </div>
    <div class="paso2" hidden>
      <h2>Articulo</h2>
      <div class="field">
        <label>Contenido:
          <textarea rows="23" name="lugar-articulo" required></textarea>
        </label>
      </div>
      <div class="ui left labeled icon button anterior" onclick="javascript:verPaso('paso1');">
        <i class="left arrow icon"></i> Anterior
      </div>
      <div class="ui right labeled icon button siguiente" onclick="javascript:verPaso('paso3');">
        <i class="right arrow icon"></i> Siguiente
      </div>
    </div>
    <div class="paso3" hidden>
      <h2>Galeria</h2>
      <label for="inputImagenes" class="ui basic button">
        <i class="add circle icon"></i><span>Agregar imagen</span>
        <input id="inputImagenes" type='file' accept="image/*" hidden>
      </label>
      <br>
      <div id="contenedorImagenes"></div>
      <br>
      <div class="ui left labeled icon button anterior" onclick="javascript:verPaso('paso2');">
        <i class="left arrow icon"></i> Anterior
      </div>
      <div class="ui right labeled icon button siguiente" onclick="javascript:verPaso('paso4');">
        <i class="right arrow icon"></i> Siguiente
      </div>
    </div>
    <div class="paso4" hidden>
      <br>
      <center>
        <button class="massive ui primary button" type="submit" id="submit" disabled>Confimar</button>
      </center>
      <br>
      <div class="ui left labeled icon button anterior" onclick="javascript:verPaso('paso3');">
        <i class="left arrow icon"></i> Anterior
      </div>
    </div>
      <footer></footer>
  </form>
</div>
<script type="text/javascript">
  /*--- Imagenes---*/
  var id_imagen = 0;
  function readFile() {
    if (this.files && this.files[0]) {
      var FR = new FileReader();
      FR.onload = function(e) {
        var item_imagen = document.createElement('div');
        item_imagen.id = "item_imagen_" + id_imagen;
        item_imagen.classList.add("previa_imagen");

        var nueva_imagen = document.createElement('img');
        nueva_imagen.src = e.target.result;

        var input_imagen = document.createElement('input');
        input_imagen.name = "galeria_imagen_" + id_imagen;
        input_imagen.value = e.target.result;
        input_imagen.hidden = true;

        var iconoX = document.createElement('i');
        iconoX.classList.add('remove');
        iconoX.classList.add('icon');
        iconoX.addEventListener('click', function() {
          $(this).parent().parent().remove();
        }, false);

        var btn_eliminar = document.createElement('div');
        btn_eliminar.classList.add("btn_imagen");
        btn_eliminar.appendChild(iconoX);
       
        item_imagen.appendChild(nueva_imagen);
        item_imagen.appendChild(input_imagen);
        item_imagen.appendChild(btn_eliminar);

        var ultimo_item = document.getElementById('contenedorImagenes').getElementsByTagName('div')[0];
        document.getElementById('contenedorImagenes').insertBefore(item_imagen, ultimo_item);

      };
      FR.readAsDataURL(this.files[0]);
    }
    id_imagen++;
  }
  document.getElementById("inputImagenes").addEventListener("change", readFile, false);
  /*-----MAP----*/
  var map = null;
  var infoWindow = null;

  function openInfoWindow(marker) {
    var markerLatLng = marker.getPosition();
    infoWindow.setContent([
      '<strong>La posicion del marcador es:</strong><br/>',
      markerLatLng.lat(),
      ', ',
      markerLatLng.lng(),
      '<br/>Arrástrame para actualizar la posición.'
    ].join(''));
    infoWindow.open(map, marker);
  }

  function load() {
    var coordenada1 = "-32.31724625010245";
    var coordenada2 = "-58.08109172661136";

    var map = new google.maps.Map(document.getElementById("map"), {
      center: new google.maps.LatLng(coordenada1, coordenada2),
      zoom: 14,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      rotateControl: false,
      mapTypeId: google.maps.MapTypeId.HYBRID
    });
    infoWindow = new google.maps.InfoWindow();

    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(coordenada1, coordenada2),
      draggable: true,
      map: map,
      title: "Seleccione Ubicacion"
    });
    google.maps.event.addListener(marker, 'dragend', function() {
      //openInfoWindow(marker);
      posicion = marker.getPosition();
      document.getElementById("lugar-ubicacion-latitud").value = posicion.lat();
      document.getElementById("lugar-ubicacion-longitud").value = posicion.lng();
      console.log("Latitud: " + posicion.lat() + "\nLongitud: " + posicion.lng());
    });
    google.maps.event.addListener(marker, 'click', function() {
      openInfoWindow(marker);
    });
  }
</script>

</body>

</html>
<?php
header('Access-Control-Allow-Origin: *');  
$rutas = [
    'Index' => [
                ],
    'Usuario' => [
        //'registro' => 'ok'
                ]
        ];

    require __DIR__ . '/vendor/autoload.php';
    require 'config.php';
    // -->Controller/Method/Params
    $url = (isset($_GET["url"])) ? $_GET["url"] : "Index/index";
    $url = explode("/", $url);
    if(isset($url[0])){$controller = ucfirst(strtolower($url[0]));}
    if(isset($url[1])){ if($url[1] != ''){ $method = $url[1];} }
    //if(isset($url[2])){ if($url[2] != ''){ $params = $url[2];} }
    $params = [];
    if(isset($url[2])){ 
        if($url[2] != ''){ 
            $x = 2;
            while(true){
                if(!isset($url[$x]) || $url[$x] == '') 
                    break;
                $params[$x - 2] = $url[$x];
                $x++;
            }
        } 
    }
    spl_autoload_register(function($class){
        if(file_exists(LIBS.$class.".php")){
            require LIBS.$class.".php";
        }
    });
    /*entrar en administrador*/
    if($controller == "Admin"){
            require './controllers/Usuario.php';
            $controller = new Usuario();
            return $controller->admin();
    }
    $path = './controllers/'.$controller.'.php';
    if(file_exists($path)){
        require $path;
        $controller = new $controller();
        
        if(isset($method)){
            if(method_exists($controller, $method)){
                $check = new ReflectionMethod(get_class($controller), $method);
                if(!$check->isPublic()){
                    //echo "Metodo Privado";
                   require './controllers/Index.php';
                   $controller = new Index();
                   $controller->index();                    
                }else{ 
                    if(isset($params[0])){
                        //$controller->{$method}($params);
                        $controller->llamarfuncion($method, $params);
                    }else{
                        $controller->{$method}();
                    }
                }
            }else{
                $viewPath = './views/'.get_class($controller).'/'.$method.'.php';
                 if(file_exists($viewPath) && isset($rutas[get_class($controller)][$method])){
                    $controller->view->render($controller,$method);
                }else{
                     if(method_exists($controller, 'index')){
                         $params = array_merge([$method], $params);
                         //$controller->index($params);
                         $controller->llamarfuncion('index', $params);
                     }else{
                        //echo "No existe metodo '<b>$method</b>' en el controlador";
                        require './controllers/Index.php';
                       $controller = new Index();
                       $controller->index();
                     }
                }
            }
        }else{
            $controller->index();
        }
    }else{
       // echo "No existe controlador '<b>$controller</b>'";
       require './controllers/Index.php';
       $controller = new Index();
       $controller->index();
    }

function verPaso(id) {
    if(id === "paso4"){
       document.getElementById("submit").disabled = false;
    }else{
        document.getElementById("submit").disabled = true;
    }
    ocultarTodo();
    for (x = 1; x < 5; x++) {
        if ($('#paso' + x).hasClass('active')) $('#paso' + x).removeClass('active');
        if (!$('#paso' + x).hasClass('disabled')) $('#paso' + x).addClass('disabled');
    }
    if (!$('#' + id).hasClass('active')) $('#' + id).addClass('active');
    if ($('#' + id).hasClass('disabled')) $('#' + id).removeClass('disabled');

    $('.' + id).fadeIn(500);
}

function ocultarTodo() {
    $('.paso1').fadeOut(0);
    $('.paso2').fadeOut(0);
    $('.paso3').fadeOut(0);
    $('.paso4').fadeOut(0);
}
$("input").keyup(function(e) {
    e.preventDefault();
});

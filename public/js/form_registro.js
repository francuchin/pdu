$(document).ready(function() {
  $('.ui.form').form({
    fields: {
      nombre: {
        identifier: 'nombre',
        rules: [{
          type: 'empty',
          prompt: 'Ingrese su nombre'
        }]
      },
      usuario: {
        identifier: 'usuario',
        rules: [{
          type: 'empty',
          prompt: 'Ingrese un usuario'
        }]
      },
      email: {
        identifier: 'email',
        rules: [{
          type: 'empty',
          prompt: 'Ingrese su e-mail'
        }, {
          type: 'email',
          prompt: 'E-mail invalido'
        }]
      },
      password: {
        identifier: 'password',
        rules: [{
          type: 'empty',
          prompt: 'Ingrese su contraseña'
        }]
      },
      repassword: {
        identifier: 'repassword',
        rules: [{
          type: 'match[password]',
          prompt: 'Contraseñas no coinciden'
        }]
      }
    }
  });
});
(alternar = function() {
    if ($("#segmentoLugares").css("display") == "none") {
        $("#segmentoIndice").fadeOut(function() {
            $("#segmentoLugares").fadeIn();
            $("#imagen_fondo").fadeIn();
            document.getElementById('boton_alternar').innerHTML = "Mostrar Inicio";
        });
    }
    else {
        $("#segmentoLugares").fadeOut(function() {
            $("#imagen_fondo").fadeOut();
            $("#segmentoIndice").fadeIn();
            document.getElementById('boton_alternar').innerHTML = "Mostrar Listado";
        });
    }
});